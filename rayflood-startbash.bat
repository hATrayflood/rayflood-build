if "%RF_USE_CYGWIN%" == "" (
	call %~dp0\rayflood-startmingw.bat
) else (
	call %~dp0\rayflood-startcygwin.bat
)
set LIB=%LIB%;%MOZ_TOOLS%\lib
%SystemRoot%\system32\xcopy %RF_SHELLDIR%\etc\profile.d\profile-rfpaths.sh %RF_SHELLDIR_ADD%\etc\profile.d\ /d /y > nul
%SystemRoot%\system32\xcopy %RF_SHELLDIR_ADD%\etc\profile.d\profile-rfpaths.sh %RF_SHELLDIR%\etc\profile.d\ /d /y > nul
if "%*"=="" (
	start /BELOWNORMAL %RF_CMD% /c %RF_SHELLDIR%\bin\bash --login -i
) else (
	start /wait /BELOWNORMAL %RF_CMD% /c %RF_SHELLDIR%\bin\rxvt -e /usr/bin/bash --login -i %*
	exit %ERRORLEVEL%
)
