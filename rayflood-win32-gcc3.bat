@setlocal
@echo off

set RF_MOZ_TOOLS_SUFFIX=-180compat
set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

echo Setting environment for using Mingw GCC3.
call %~dp0\extratools\gcc-mingw\mingw-gcc\mingw32.bat

call %~dp0\rayflood-startbash.bat %*

@endlocal
