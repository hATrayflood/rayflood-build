@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%VCToolkitInstallDir%"=="" (
	echo Microsoft Visual C++ Toolkit 2003 was not found. Exiting.
	pause
	exit /b 1
)
if "%RF_DOTNETSDKDIR%"=="" (
	echo Microsoft .NET Framework SDK v1.1 was not found. Exiting.
	pause
	exit /b 1
)
if "%RF_PSDKDIR%"=="" (
	echo Microsoft Platform SDK for Windows Server 2003 SP1 or R2 was not found. Exiting.
	pause
	exit /b 1
)

call "%RF_DOTNETSDKDIR%\Bin\sdkvars.bat"
call "%VCToolkitInstallDir%\vcvars32.bat"
call %~dp0\rayflood-atlmfc-psdk.bat
set PATH=%RF_PSDKDIR%\bin;%PATH%
set PATH_SYS=%SystemDrive%\masm32\BIN;%PATH_SYS%
set LIB=%LIB%;%RF_PSDKDIR%\lib;%~dp0\vc71e-msvcprt;%~dp0\comsupp_compat\x86
set INCLUDE=%INCLUDE%;%RF_PSDKDIR%\include;%RF_PSDKDIR%\include\crt

call %~dp0\rayflood-startbash.bat %*

@endlocal
