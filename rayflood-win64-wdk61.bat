@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_WDK61DIR%"=="" (
	echo Microsoft Windows Driver Kits 6.1 6001.18002 was not found. Exiting.
	pause
	exit /b 1
)
if "%RF_PSDKDIR%"=="" (
	echo Microsoft Platform SDK for Windows Server 2003 SP1 or R2 was not found. Exiting.
	pause
	exit /b 1
)

rem call "%RF_WDK61DIR%\bin\setenv.bat" %RF_WDK61DIR% fre AMD64 WLH
set PATH=%RF_WDK61DIR%\bin\x86\amd64;%RF_WDK61DIR%\bin\x86;%RF_PSDKDIR%\Bin;%PATH%
set LIB=%RF_WDK61DIR%\lib\wlh\amd64;%RF_WDK61DIR%\lib\Crt\amd64;%RF_WDK61DIR%\lib\ATL\amd64;%RF_WDK61DIR%\lib\Mfc\amd64;%~dp0\comsupp_compat\x64-mt;%LIB%
set INCLUDE=%RF_WDK61DIR%\inc\api;%RF_WDK61DIR%\inc\crt;%RF_WDK61DIR%\inc\api\crt\stl60;%RF_WDK61DIR%\inc\atl30;%RF_WDK61DIR%\inc\mfc42;%RF_PSDKDIR%\Include;%INCLUDE%
set USE_STATIC_LIBS=1
set CL=-D_CRT_SECURE_FORCE_DEPRECATE -D_CRT_NONSTDC_FORCE_DEPRECATE

call %~dp0\rayflood-startbash.bat %*

@endlocal
