#!/bin/bash

. ${RF_TOOLKIT}/buildshell/setdirs.sh

RF_APP_VER=`basename ${1}`
RF_SRC_APP_VER=${RF_APP_VER#*:}
RF_APP_VER=${RF_APP_VER%:*}
RF_MSVCVERSION=${2}

RF_APP_VER=${RF_APP_VER} RF_MSVCVERSION=${RF_MSVCVERSION} . ${RF_MOZCONFIG}/mozconfig.sh > /dev/null

if [ $? -ne 0 ] ; then
	echo "${RF_APP_VER} or ${RF_MSVCVERSION} ?"
	exit
fi

echo "RF_SRC_URL=${RF_SRC_URL}"
echo "RF_BUILD_CMD=${RF_BUILD_CMD}"
if [ "${RF_DONTBUILD}" ] ; then
	echo "RF_DONTBUILD=${RF_DONTBUILD}"
fi
if [ "${RF_PGO}" ] && [ ! "${RF_DONT_PGO}" ] ; then
	echo "RF_PGO=${RF_PGO}"
fi
if [ "${RF_L10N}" ] ; then
	echo "RF_L10N=${RF_L10N}"
fi
if [ "${RF_PATCHES_EXTRA}" ] ; then
	echo "RF_PATCHES_EXTRA=${RF_PATCHES_EXTRA}"
fi
if [ "${RF_OPT_CPUOFF}" ] ; then
	echo "RF_OPT_CPUOFF=${RF_OPT_CPUOFF}"
fi
if [ "${RF_MSVC_MIN_WINVER}" ] ; then
	echo "RF_MSVC_MIN_WINVER=${RF_MSVC_MIN_WINVER}"
fi
if [ "${RF_MSVC_WINSDK_VER}" ] ; then
	echo "RF_MSVC_WINSDK_VER=${RF_MSVC_WINSDK_VER}"
fi
