#!/bin/bash

BUILDROOT=${1}
L10NREP=${2}
RF_APP_VER=${3}
L10N=${4}
L10N_TBZ=${5}
PROJECT=${RF_APP_VER%-*}
VERSION=${RF_APP_VER##*-}
shift ; shift ; shift ; shift ; shift

curl --help > /dev/null 2>&1
if [ ${?} -eq 0 ] ; then
	WGET="curl -O"
else
	WGET="wget"
fi

if [ "${PROJECT}" = "lightning" ] && [ "${VERSION}" = "1.0b2" ] ; then
	PROJECT="CALENDAR"
fi

L10N_LOG=co-l10n.log
L10N_TAG=`echo ${PROJECT} | tr [:lower:] [:upper:]`_`echo ${VERSION} | tr . _`_RELEASE

if [ "${L10NREP}" = "-" ] ; then
	cvs -d :pserver:anonymous@cvs-mirror.mozilla.org:/l10n export -r ${L10N_TAG} l10n/${L10N} >> ${L10N_LOG} 2>&1
else
	L10NREPROOT=${BUILDROOT}/l10nreps/${L10NREP}
	L10NREPDIR=${L10NREPROOT}/${L10N}
	HG_URL=http://hg.mozilla.org/releases/${L10NREP}/${L10N}/

	mkdir -p ${L10NREPROOT}
	if [ ! -e ${L10NREPDIR} ] ; then
		hg clone ${HG_URL} ${L10NREPDIR} >> ${L10N_LOG} 2>&1
	else
		hg -R ${L10NREPDIR} pull -u ${HG_URL} >> ${L10N_LOG} 2>&1
		hg -R ${L10NREPDIR} update -C tip >> ${L10N_LOG} 2>&1
	fi
	hg -R ${L10NREPDIR} update -C ${L10N_TAG} >> ${L10N_LOG} 2>&1
	hg -R ${L10NREPDIR} archive l10n/${L10N} >> ${L10N_LOG} 2>&1
fi

if [ $? -eq 0 ] && [ -e l10n ] ; then
	tar jcf ${L10N_TBZ} l10n > /dev/null 2>&1
	rm -fr l10n
else
	${WGET} http://rayflood.org/mozilla/${L10N_TBZ} >> ${L10N_LOG} 2>&1
fi
