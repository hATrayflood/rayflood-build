#!/bin/sh

VERSION=2.0.7.20100830

cat <<EOF > chrome.manifest
locale inspector ja jar:chrome/inspector.jar!/locale/ja/inspector/
locale inspector ja-JP-mac jar:chrome/inspector.jar!/locale/ja-JP-mac/inspector/
EOF

cat <<EOF > install.rdf
<?xml version="1.0"?>
<RDF xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	xmlns:em="http://www.mozilla.org/2004/em-rdf#">

<!-- http://hg.mozilla.org/mozilla-central/file/tip/browser/locales/generic/install.rdf -->

<Description about="urn:mozilla:install-manifest">
	<em:id>inspector-ja@mozilla.l10n.jp</em:id>
	<em:name>DOM Inspector Development Languagepack (ja)</em:name>
	<em:version>${VERSION}</em:version>
	<em:type>8</em:type>
	
	<!-- require DOM Inspector 2.0.7 -->
	<em:requires>
		<Description>
			<em:id>inspector@mozilla.org</em:id>
			<em:minVersion>2.0.7</em:minVersion>
			<em:maxVersion>2.0.*</em:maxVersion>
		</Description>
	</em:requires>
	
	<em:targetApplication>
		<!-- Firefox -->
		<Description>
			<em:id>{ec8030f7-c20a-464f-9b0e-13a3a9e97384}</em:id>
			<em:minVersion>3.0</em:minVersion>
			<em:maxVersion>4.0.*</em:maxVersion>
		</Description>
	</em:targetApplication>
	
	<em:targetApplication>
		<!-- Thunderbird -->
		<Description>
			<em:id>{3550f703-e582-4d05-9a08-453d09bdfdc6}</em:id>
			<em:minVersion>3.0</em:minVersion>
			<em:maxVersion>3.3.*</em:maxVersion>
		</Description>
	</em:targetApplication>
	
	<em:targetApplication>
		<!-- Sunbird -->
		<Description>
			<em:id>{718e30fb-e89b-41dd-9da7-e25a45638b28}</em:id>
			<em:minVersion>1.0</em:minVersion>
			<em:maxVersion>1.1.*</em:maxVersion>
		</Description>
	</em:targetApplication>
	
	<em:targetApplication>
		<!-- Suite -->
		<Description>
			<em:id>{92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}</em:id>
			<em:minVersion>2.0</em:minVersion>
			<em:maxVersion>2.1.*</em:maxVersion>
		</Description>
	</em:targetApplication>
	
	<em:targetApplication>
		<!-- Fennec -->
		<Description>
			<em:id>{a23983c0-fd0e-11dc-95ff-0800200c9a66}</em:id>
			<em:minVersion>1.0</em:minVersion>
			<em:maxVersion>4.0.*</em:maxVersion>
		</Description>
	</em:targetApplication>
	
	<em:targetApplication>
		<!-- Toolkit -->
		<Description>
			<em:id>toolkit@mozilla.org</em:id>
			<em:minVersion>1.9</em:minVersion>
			<em:maxVersion>2.0.*</em:maxVersion>
		</Description>
	</em:targetApplication>
	
	<em:description>DOM Inspector Language Package (ja) - Development Edition</em:description>
	<em:creator>Mozilla Japanese L10N Community</em:creator>
	
	<em:homepageURL>http://trac.mozilla.l10n.jp/</em:homepageURL>
	
</Description>
</RDF>
EOF

filter(){
python - <<EOF $*
import sys
import os
import os.path

def getFilters(lang, filterfile):
	filters = {}
	with open(filterfile) as file:
		for line in file:
			if line.lstrip().startswith(lang + "."):
				name, value = line[len(lang) + 1:len(line)].split("=", 1)
				filters["@@" + name.strip() + "@@"] = value.strip()
	return filters

def srcToDst(src, dst):
	for f in os.listdir(src):
		s = src + "/" + f
		d = dst + "/" + f
		if os.path.isdir(s):
			srcToDst(s, d)
		else:
			filter(s, d)

def filter(src, dst):
	if not os.path.exists(os.path.dirname(dst)):
		os.makedirs(os.path.dirname(dst))
	with open(src) as srcfile:
		d = srcfile.read()
		for k in filters:
			d = d.replace(k, filters[k])
		with open(dst, "wb") as dstfile:
			dstfile.write(d)

if __name__ == "__main__":
	if len(sys.argv) <= 4:
		print "syntax: python langpack-filter.py ja-JP-mac ja.filters inspector/chrome locale/ja-JP-mac/inspector"
		sys.exit()

	lang = sys.argv[1]
	filterfile = sys.argv[2]
	src = sys.argv[3]
	dst = sys.argv[4]

	filters = getFilters(lang, filterfile)
	srcToDst(src, dst)
EOF
}

svn export http://svn.mozilla.l10n.jp/trunk/ja.filters
svn export http://svn.mozilla.l10n.jp/trunk/ja/extensions/inspector/
filter ja ja.filters inspector/chrome locale/ja/inspector
filter ja-JP-mac ja.filters inspector/chrome locale/ja-JP-mac/inspector
mkdir chrome
zip -0r chrome/inspector.jar locale
zip -9r domi-langpack-ja-${VERSION}.xpi chrome chrome.manifest install.rdf

rm -rf inspector locale chrome ja.filters chrome.manifest install.rdf
