#!/bin/bash

SRCDIR=${1}
shift

curl --help > /dev/null 2>&1
if [ ${?} -eq 0 ] ; then
	WGET="curl -O"
else
	WGET="wget"
fi

mkdir -p ${SRCDIR}
for SRC_URL in $* ;  do
	SRC_TBZ=${SRC_URL##*/}
	LOCAL_SRC=${SRCDIR}/${SRC_TBZ}
	if [ ! -e "${LOCAL_SRC}" ] ; then
		${WGET} ${SRC_URL} >> wget.log 2>&1
		mv ${SRC_TBZ} ${SRCDIR}
	fi
	tar jxf ${LOCAL_SRC}
done
