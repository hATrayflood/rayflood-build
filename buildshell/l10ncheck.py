#!/usr/bin/env python
import sys
import os
import os.path

def recur(f, ab, cd):
	if os.path.isfile(f):
		abf = f
		cdf = os.path.join(cd, os.path.relpath(f, ab))
		suffix = f.split(".")[-1]
		if suffix == "properties":
			prop(abf, cdf)
		elif suffix == "dtd":
			dtd(abf, cdf)
		else:
			other(abf, cdf)
	if os.path.isdir(f):
		for d in os.listdir(f):
			recur(os.path.join(f, d), ab, cd)

def prop(ab, cd):
	if not existcheck(ab, cd):
		return

	abp = readprop(ab)
	cdp = readprop(cd)
	printsortbynumber(ab, abp, cdp)

def readprop(f):
	entities = {}
	number = 0
	with open(f) as file:
		for line in file:
			number += 1
			if not (line.isspace() or line.lstrip().startswith("#")):
				(key, value) = line.strip().split("=", 1)
				entities[key.strip()] = number
	return entities

def dtd(ab, cd):
	if not existcheck(ab, cd):
		return

	abp = readdtd(ab)
	cdp = readdtd(cd)
	printsortbynumber(ab, abp, cdp)

def readdtd(f):
	entities = {}
	number = 0
	with open(f) as file:
		for line in file:
			number += 1
			if line.lstrip().startswith("<!ENTITY"):
				key = line.strip().split()[1]
				entities[key.strip()] = number
	return entities

def printsortbynumber(f, abp, cdp):
	bynumber = {}
	for key in abp:
		if not cdp.has_key(key):
			bynumber[abp[key]] = key
	for number in sorted(bynumber):
		print(relpathbyenus(f) + ":" + str(number) + ":\n\t" + bynumber[number])

def relpathbyenus(f):
	en_US = "locales/en-US"
	index = f.find(en_US)
	if index > -1:
		f = f[index + len(en_US) + 1:len(f)]
	return f

def existcheck(ab, cd):
	if not os.path.exists(ab):
		print(ab)
		return False
	if not os.path.exists(cd):
		print(cd)
		return False
	return True

def other(ab, cd):
	if not existcheck(ab, cd):
		return

if __name__ == "__main__":
	if len(sys.argv) <= 2:
		print("en-US ab-CD")
		sys.exit()

	ab = sys.argv[1]
	cd = sys.argv[2]

	recur(ab, ab, cd)
