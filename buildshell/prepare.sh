#!/bin/bash

. ${RF_TOOLKIT}/buildshell/setdirs.sh

RF_APP_VER=`basename ${1}`
RF_SRC_APP_VER=${RF_APP_VER#*:}
RF_APP_VER=${RF_APP_VER%:*}
RF_PROJECT=${RF_APP_VER%-*}
RF_VERSION=${RF_APP_VER##*-}
RF_VERSION_MAJOR=`echo ${RF_VERSION} | awk -F . '{print $1}'`
RF_VERSION_MINOR=`echo ${RF_VERSION} | awk -F . '{print $2}'`
if [ ${RF_VERSION_MAJOR} -eq 24 ] ; then
RF_VERSION_MINOR=0
fi
RF_CANDIDATE=${2}
if [ -e ${BUILDROOT}/${RF_APP_VER} ] ; then
	echo "${RF_APP_VER} is already exist."
	exit 1
fi

RF_APP_VER=${RF_SRC_APP_VER} . ${RF_MOZCONFIG}/mozconfig.sh
if [ $? -ne 0 ] ; then
	echo "${RF_APP_VER} or ${RF_MSVCVERSION} ?"
	exit 1
fi

echo "prepare for ${RF_APP_VER}"
mkdir -p ${BUILDROOT}/${RF_APP_VER}
mkdir -p ${SRCDIR}/${RF_APP_VER}

if [ "${RF_CANDIDATE}" ] ; then
	rm -f ${SRCDIR}/${RF_SRC_URL##*/}
fi

cd ${BUILDROOT}/${RF_APP_VER}
${SHELLDIR}/wgetsource.sh ${SRCDIR} ${RF_SRC_URL}
if [ ! -e "${RF_SRCDIR}" ] ; then
	echo "${RF_APP_VER} not found."
	exit 1
fi

if [ ${RF_SRC_SUBDIR} != "." ] ; then
	RF_SUBDIR_PATCH="`ls ${PTCDIR}/firefox-${RF_VERSION_MAJOR}.${RF_VERSION_MINOR}-*.patch 2>/dev/null`"
	echo "${RF_SUBDIR_PATCH}" > ${RF_SUBDIR_PATCH##*/}.log
	(cd ${RF_SRCDIR}/${RF_SRC_SUBDIR} && patch -p1 --no-backup-if-mismatch -i ${RF_SUBDIR_PATCH}) >> ${RF_SUBDIR_PATCH##*/}.log
fi
if [ "${RF_APP_VER}" = "${RF_SRC_APP_VER}" ] ; then
	rm -f  ${RF_SRCDIR}/${RF_SRC_SUBDIR}/python/psutil/*.so
	rm -f  ${RF_SRCDIR}/${RF_SRC_SUBDIR}/python/psutil/*.pyd
	rm -fr ${RF_SRCDIR}/${RF_SRC_SUBDIR}/python/psutil/build/
	mv ${RF_SRCDIR} ${RF_SRCDIR}.orig
	chmod -R -w ${RF_SRCDIR}.orig
	${SHELLDIR}/wgetsource.sh ${SRCDIR} ${RF_SRC_URL}
	if [ ${RF_SRC_SUBDIR} != "." ] ; then
		(cd ${RF_SRCDIR}/${RF_SRC_SUBDIR} && patch -p1 --no-backup-if-mismatch -i ${RF_SUBDIR_PATCH}) >> /dev/null
	fi
fi

if [ "${RF_L10N}" ] ; then
	${SHELLDIR}/get-l10n.sh ${SRCDIR} ${RF_SRC_APP_VER} ${RF_L10NREP} ${RF_SRCDIR} ${BUILDROOT} ${RF_L10N}
else
	mkdir -p ${BUILDROOT}/${RF_APP_VER}/l10n
fi

RF_PATCHES="`ls ${PTCDIR}/${RF_PROJECT}-${RF_VERSION_MAJOR}.${RF_VERSION_MINOR}-*.patch 2>/dev/null`"
for RF_PATCHE_EXTRA in ${RF_PATCHES_EXTRA} ; do
	RF_PATCHES="${RF_PATCHES} ${PTCDIR}/${RF_PATCHE_EXTRA}"
done
if [ "${RF_PATCHES}" ] ; then
	${SHELLDIR}/applypatches.sh ${RF_PATCHES}
fi
