import sys
import smtplib
import email.mime.text
import mimetypes

def sendmail(server, from_, to, subject, message, charset):
	msg = email.mime.text.MIMEText(message, _charset=charset)
	msg['Subject'] = subject
	msg['From'] = from_
	msg['To'] = to

	smtp = smtplib.SMTP(server)
	smtp.sendmail(from_, [to], msg.as_string())
	smtp.close()

def main():
	server  = sys.argv[1]
	from_   = sys.argv[2]
	to      = sys.argv[3]
	subject = sys.argv[4]

	fp = open(sys.argv[5], 'rb')
	message = fp.read()
	fp.close()
	sendmail(server, from_, to, subject, message, None)

if __name__ == '__main__':
	if len(sys.argv) <= 5:
		print('sendmail.py server[:port] from to subject /path/to/message')
		exit()
	main()
