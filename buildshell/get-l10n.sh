#!/bin/bash

SRCDIR=${1}
RF_APP_VER=${2}
L10NREP=${3}
SRC_SUBDIR=${4}
BUILDROOT=${5}
shift ; shift ; shift ; shift ; shift

mkdir -p ${SRCDIR}/${RF_APP_VER}
for L10N in $* ;  do
	mkdir -p ${SRCDIR}/l10n/${L10N}
	L10N_TBZ=${RF_APP_VER}-l10n-${L10N}.tar.bz2
	LOCAL_SRC=${SRCDIR}/${RF_APP_VER}/${L10N_TBZ}
	if [ ! -e "${LOCAL_SRC}" ] ; then
		`dirname ${0}`/co-l10n.sh ${BUILDROOT} ${L10NREP} ${RF_APP_VER} ${L10N} ${L10N_TBZ}
		if [ -e "${L10N_TBZ}" ] ; then
			cp -p ${L10N_TBZ} ${SRCDIR}/l10n/${L10N}
			mv ${L10N_TBZ} ${SRCDIR}/${RF_APP_VER}
		fi
	fi
done

for L10N in $* ;  do
	L10N_TBZ=${RF_APP_VER}-l10n-${L10N}.tar.bz2
	LOCAL_SRC=${SRCDIR}/l10n/${L10N}/${L10N_TBZ}
	if [ -e "${LOCAL_SRC}" ] ; then
		tar jxf ${LOCAL_SRC} > /dev/null 2>&1
	fi
done
