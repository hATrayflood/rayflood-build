#!/bin/bash

EXTRA=${1}
BINDIR=${2}
CHROME=${BINDIR}/chrome

UNAME_S=`uname -s`
UNAME_M=`uname -m`

case ${UNAME_S} in
MINGW32_*|CYGWIN_*)
	PLATFORM=win
;;
Darwin)
	PLATFORM=mac
;;
*)
	PLATFORM=unix
;;
esac

cp -p ${EXTRA}/*.jar ${CHROME}
cp -p ${EXTRA}/installed-chrome.txt /tmp/installed-chrome.txt
cat ${CHROME}/installed-chrome.txt >> /tmp/installed-chrome.txt
cp -p /tmp/installed-chrome.txt ${CHROME}/installed-chrome.txt
rm -f /tmp/installed-chrome.txt

cp -p ${EXTRA}/${PLATFORM}/*.jar ${CHROME}
cp -p ${EXTRA}/${PLATFORM}/installed-chrome.txt /tmp/installed-chrome.txt
cat ${CHROME}/installed-chrome.txt >> /tmp/installed-chrome.txt
cp -p /tmp/installed-chrome.txt ${CHROME}/installed-chrome.txt
rm -f /tmp/installed-chrome.txt

cp -pr ${EXTRA}/bin/* ${BINDIR}
