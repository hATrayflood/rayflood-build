#!/bin/bash

LANG=C diff -urN -x 'CVS' -x '*.pyc' -x '*.pyo' -x '*.stackdump' -x 'configure' -x '.mozconfig.mk*' -x '.mozconfig.out' -x '*.egg-info' ${1}.orig ${1} >> ${2}
