#!/bin/bash

for PATCH in $* ; do
	echo "${PATCH}" > ${PATCH##*/}.log
	patch -p0 --no-backup-if-mismatch -i ${PATCH} >> ${PATCH##*/}.log
done
