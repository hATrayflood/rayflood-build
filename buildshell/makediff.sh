#!/bin/bash

. ${RF_TOOLKIT}/buildshell/setdirs.sh

RF_APP_VER=`basename ${1}`
RF_SRC_APP_VER=${RF_APP_VER#*:}
RF_APP_VER=${RF_APP_VER%:*}
RF_SRCDIR=${2}
RF_SRC_SUBDIR=${3}
RF_PROJECT=${RF_APP_VER%-*}
RF_VERSION=${RF_APP_VER##*-}
RF_VERSION_MAJOR=`echo ${RF_VERSION} | awk -F . '{print $1}'`
RF_VERSION_MINOR=`echo ${RF_VERSION} | awk -F . '{print $2}'`
if [ ${RF_VERSION_MAJOR} -eq 24 ] ; then
RF_VERSION_MINOR=0
fi
if [ ! "${RF_SRCDIR}" ] ; then
	. ${RF_MOZCONFIG}/mozconfig.sh
fi

echo "make patch for ${RF_APP_VER}."
cd ${BUILDROOT}/${RF_APP_VER}
RF_PATCH=${RF_APP_VER}-rayflood.patch
rm -f ${RF_SRCDIR}/.mozconfig
rm -f ${RF_SRCDIR}/${RF_SRC_SUBDIR}/toolkit/xre/nsWindowCreator.cpp
rm -f ${RF_SRCDIR}/${RF_SRC_SUBDIR}/toolkit/xre/showOSAlert.cpp
rm -f ${RF_SRCDIR}/${RF_SRC_SUBDIR}/xpcom/idl-parser/xpidllex.py
rm -f ${RF_SRCDIR}/${RF_SRC_SUBDIR}/xpcom/idl-parser/xpidlyacc.py
rm -f ${RF_SRCDIR}/${RF_SRC_SUBDIR}/xpcom/idl-parser/*.pyc
rm -f  ${RF_SRCDIR}/${RF_SRC_SUBDIR}/python/psutil/*.so
rm -f  ${RF_SRCDIR}/${RF_SRC_SUBDIR}/python/psutil/*.pyd
rm -fr ${RF_SRCDIR}/${RF_SRC_SUBDIR}/python/psutil/build/
rm -f  ${RF_SRCDIR}.orig/${RF_SRC_SUBDIR}/python/psutil/*.so
rm -f  ${RF_SRCDIR}.orig/${RF_SRC_SUBDIR}/python/psutil/*.pyd
rm -fr ${RF_SRCDIR}.orig/${RF_SRC_SUBDIR}/python/psutil/build/
rm -f ${RF_PATCH}
${SHELLDIR}/difforiginal.sh ${RF_SRCDIR} ${RF_PATCH}

case ${RF_MSVCVERSION} in
win32-*|win64-*)
	mkdir -p ${SRCDIR}/${RF_APP_VER}
	cp -p ${RF_PATCH} ${SRCDIR}/${RF_APP_VER}
	cp -p ${RF_PATCH} ${PTCDIR}/${RF_PROJECT}-${RF_VERSION_MAJOR}.${RF_VERSION_MINOR}-rayflood.patch
;;
esac
