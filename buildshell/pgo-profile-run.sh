#!/bin/bash

. ${RF_TOOLKIT}/buildshell/setdirs.sh

RF_APP_VER=`basename ${1}`
RF_SRC_APP_VER=${RF_APP_VER#*:}
RF_APP_VER=${RF_APP_VER%:*}
RF_PROJECT=${RF_APP_VER%-*}
RF_VERSION=${RF_APP_VER##*-}
RF_THREADS=${2}
if [ -z "${RF_THREADS}" ] ; then
RF_THREADS=1
fi
RF_OBJDIR=../${RF_MSVCVERSION}

. ${RF_MOZCONFIG}/mozconfig.sh
if [ $? -ne 0 ] ; then
	echo "${RF_APP_VER} or ${RF_MSVCVERSION} ?"
	exit
fi
if [ "${RF_DONTBUILD}" ] ; then
	echo "${RF_APP_VER} on ${RF_MSVCVERSION} is not available. **RUN FORCE**."
fi
if [ ! -s ${BUILDROOT}/${RF_APP_VER}/${RF_SRCDIR}/.mozconfig ] ; then
	echo "${RF_APP_VER} is not found."
	exit
fi

export PYMAKE_PATH=$(cd ${BUILDROOT}/${RF_APP_VER}/${RF_SRCDIR}/${RF_SRC_SUBDIR}/build/pymake && pwd)

cd ${BUILDROOT}/${RF_APP_VER}/${RF_SRCDIR}
mkdir -p ${RF_OBJDIR}
rm -f ${RF_OBJDIR}/jarlog/en-US.log
RF_APP_VER=${RF_APP_VER} \
RF_OBJDIR=${RF_OBJDIR} \
RF_THREADS=${RF_THREADS} \
RF_INPROGRESS=1 \
RUN_AUTOCONF_LOCALLY=1 \
CONFIG_SHELL=${CONFIG_SHELL} \
MOZ_PGO_INSTRUMENTED=1 JARLOG_FILE=jarlog/en-US.log EXTRA_TEST_ARGS=10 ${RF_MAKE_CMD} -C ${RF_OBJDIR} pgo-profile-run
exit $?
