#!/bin/bash

SHELLDIR=${RF_TOOLKIT}/buildshell
. ${SHELLDIR}/apps.sh
PREPARE_EXECUTED=0
PREPARE_NEEDED=0
for APP in ${APPS} ; do
	${SHELLDIR}/prepare.sh ${APP}
	PREPARE_EXECUTED=$((${PREPARE_EXECUTED} + ${?}))
	PREPARE_NEEDED=$((${PREPARE_NEEDED} + 1))
done

if [ ${PREPARE_EXECUTED} -eq ${PREPARE_NEEDED} ] ; then
	exit
fi

mkdir -p ${RF_TOOLKIT}/../mozilla
BUILDROOT=$(cd ${RF_TOOLKIT}/../mozilla && pwd)
rm -f ${BUILDROOT}/failed.log

PYMAKE_PATH=${BUILDROOT}/pymake
if [ -d ${PYMAKE_PATH} ] ; then
	(cd ${PYMAKE_PATH} && hg pull)
	(cd ${PYMAKE_PATH} && hg update -c)
else
	(cd ${BUILDROOT} && hg clone https://hg.mozilla.org/users/bsmedberg_mozilla.com/pymake/)
fi
