#!/bin/bash

. ${RF_TOOLKIT}/buildshell/setdirs.sh

RF_APP_VER=`basename ${1}`
RF_SRC_APP_VER=${RF_APP_VER#*:}
RF_APP_VER=${RF_APP_VER%:*}
RF_PROJECT=${RF_APP_VER%-*}
RF_VERSION=${RF_APP_VER##*-}
RF_THREADS=${2}
if [ -z "${RF_THREADS}" ] ; then
RF_THREADS=1
fi
RF_OBJDIR=../${RF_MSVCVERSION}

. ${RF_MOZCONFIG}/mozconfig.sh
if [ $? -ne 0 ] ; then
	echo "${RF_APP_VER} or ${RF_MSVCVERSION} ?"
	exit
fi
if [ "${RF_DONTBUILD}" ] ; then
	echo "${RF_APP_VER} on ${RF_MSVCVERSION} is not available. **RUN FORCE**."
fi

cd ${BUILDROOT}/${RF_APP_VER}/${RF_SRCDIR}
if [ -e ${RF_OBJDIR}/build.done ] ; then
	echo "${RF_APP_VER} on ${RF_MSVCVERSION} is already build finished."
	exit
fi

RF_GECKO_VERSION=`perl ${RF_SRC_SUBDIR}/config/milestone.pl -topsrcdir ${RF_SRC_SUBDIR}`
for L10N_DIR in `ls ../l10n` ; do
	for L10N in ${RF_L10N} ; do
		if [ "${L10N_DIR}" = "${L10N}" ] ; then
			continue 2
		fi
	done
	rm -rf ../l10n/${L10N_DIR}
done

echo "build ${RF_APP_VER} ${RF_OBJDIR}"
cp -p ${RF_TOOLKIT}/mozconfig/.mozconfig ${BUILDROOT}/${RF_APP_VER}/${RF_SRCDIR}

TAKE_RENAME=${RF_OBJDIR}/build.log
if [ "${RF_PGO}" ] && [ ! "${RF_DONT_PGO}" ] ; then
TAKE_RENAME=${RF_OBJDIR}
fi
if [ -e ${TAKE_RENAME} ] ; then
	TAKE=0
	while [ -e ${TAKE_RENAME}.${TAKE} ] ; do
		TAKE=$((${TAKE} + 1))
	done
	mv ${TAKE_RENAME} ${TAKE_RENAME}.${TAKE}
fi

mkdir -p ${RF_OBJDIR}
rm -f  ${RF_SRC_SUBDIR}/python/psutil/*.so
rm -f  ${RF_SRC_SUBDIR}/python/psutil/*.pyd
rm -fr ${RF_SRC_SUBDIR}/python/psutil/build/

env | sort > ${RF_OBJDIR}/env.txt 2>&1
RF_APP_VER=${RF_APP_VER} \
RF_OBJDIR=${RF_OBJDIR} \
RF_THREADS=${RF_THREADS} \
RF_INPROGRESS=1 \
${CONFIG_SHELL} ./configure --help > ${RF_OBJDIR}/configure.help 2>&1

if [ "${RF_PYMAKE}" ] ; then
	export PYMAKE_PATH=$(cd ${BUILDROOT}/${RF_APP_VER}/${RF_SRCDIR}/${RF_SRC_SUBDIR}/build/pymake && pwd)
fi

RF_APP_VER=${RF_APP_VER} \
RF_OBJDIR=${RF_OBJDIR} \
RF_THREADS=${RF_THREADS} \
RF_INPROGRESS=1 \
RUN_AUTOCONF_LOCALLY=1 \
CONFIG_SHELL=${CONFIG_SHELL} \
${RF_BUILD_CMD} > ${RF_OBJDIR}/build.log 2>&1

ERROR=$?
if [ ${ERROR} -ne 0 ] ; then
	echo ${RF_APP_VER}_${RF_MSVCVERSION} >> ${BUILDROOT}/failed.log
	echo ${RF_APP_VER}_${RF_MSVCVERSION} >> ${BUILDROOT}/${RF_APP_VER}/failed_${RF_MSVCVERSION}.log
	exit ${ERROR}
fi

rm -f ${BUILDROOT}/${RF_APP_VER}/failed_${RF_MSVCVERSION}.log
rm -f ${BUILDROOT}/${RF_APP_VER}/build_broken_${RF_MSVCVERSION}.log
APP_NAME=`grep ^MOZ_APP_NAME ${RF_OBJDIR}/config/autoconf.mk | awk '{print $3}'`
BUILD_APP=`grep ^MOZ_BUILD_APP ${RF_OBJDIR}/config/autoconf.mk | awk '{print $3}'`
RF_BUILDNAME=rayflood-${RF_MSVCVERSION}
ARCHIVE_NAME=${APP_NAME}-${RF_VERSION}-${RF_BUILDNAME}
if [ -z "${RF_INSTALLER}" ] ; then
	RF_INSTALLER="${BUILD_APP}/installer"
fi

cd ${RF_OBJDIR}
RF_OBJDIR=`pwd`
grep -rn warning build.log* > warning.log
grep -rn O1 build.log* > O1.log
grep -rn O2 build.log* > O2.log
grep -rn O3 build.log* > O3.log
grep -rn Os build.log* > Os.log
grep -rn Ox build.log* > Ox.log
grep -rn -- -GL build.log* > GL.log
grep -rn arch: build.log* > arch.log
grep -rn favor: build.log* > favor.log
grep -rn march build.log* > march.log
grep -rn mtune build.log* > mtune.log
grep -rn mno-cygwin build.log* > mno-cygwin.log
grep -rn mwindows build.log* > mwindows.log
grep -rn fprofile build.log* > fprofile.log
grep -rni MANIFEST build.log* > MANIFEST.log
grep -rni DYNAMICBASE build.log* > DYNAMICBASE.log
grep -rni LARGEADDRESSAWARE build.log* > LARGEADDRESSAWARE.log
grep -rni LTCG build.log* > LTCG.log
grep -rni SUBSYSTEM build.log* > SUBSYSTEM.log
${RF_MAKE_CMD} -C ${RF_INSTALLER} > install.log 2>&1

ERROR=$?
if [ ${ERROR} -ne 0 ] ; then
	echo ${RF_APP_VER}_${RF_MSVCVERSION} >> ${BUILDROOT}/failed.log
	echo ${RF_APP_VER}_${RF_MSVCVERSION} >> ${BUILDROOT}/${RF_APP_VER}/failed_${RF_MSVCVERSION}.log
	exit
fi

cd ${RF_SRC_SUBDIR}/dist
if [ -f bin/chrome/toolkit/content/global/buildconfig.html ] ; then
	cp -p bin/chrome/toolkit/content/global/buildconfig.html .
else
	unzip -j bin/chrome/toolkit.jar content/global/buildconfig.html > /dev/null 2>&1
fi
mv -f buildconfig.html ${ARCHIVE_NAME}.buildconfig.html

case ${RF_MSVCVERSION} in
mac_*)
	CHROME_DIR=${APP_NAME}/${APP_NAME}.app/Contents/MacOS
;;
*)
	CHROME_DIR=${APP_NAME}
;;
esac
NPMOZAX=npmozax-${RF_GECKO_VERSION}
if [ -f xpi-stage/${NPMOZAX}.xpi ] ; then
	cp -p xpi-stage/${NPMOZAX}.xpi ${NPMOZAX}-fx+sm-${RF_BUILDNAME}.xpi
fi
if [ -f xpi-stage/lightning*.xpi ] ; then
	cp -p xpi-stage/lightning*.xpi lightning-${RF_LIGHTNING_VERSION:-${RF_VERSION}}-${RF_LIGHTNING_TARGET}-${RF_BUILDNAME}.xpi
fi
EXTRA_CHROME_DIR=${BUILDROOT}/${RF_APP_VER}/extra-chrome
if [ -e ${EXTRA_CHROME_DIR} ] ; then
	for EXTRA_CHROME in `ls ${EXTRA_CHROME_DIR}` ; do
		${SHELLDIR}/extrachrome.sh ${EXTRA_CHROME_DIR}/${EXTRA_CHROME} ${CHROME_DIR} >> ${RF_OBJDIR}/install.log 2>&1
	done
fi
case ${RF_MSVCVERSION} in
mac_*)
if [ "${APP_NAME}" = "xulrunner" ] ; then
	$(cd ${APP_NAME} && archive ${RF_MSVCVERSION} ../${ARCHIVE_NAME} XUL.framework >> ${RF_OBJDIR}/install.log 2>&1)
else
	$(cd ${APP_NAME} && archive ${RF_MSVCVERSION} ../${ARCHIVE_NAME} ${APP_NAME}.app >> ${RF_OBJDIR}/install.log 2>&1)
fi
;;
*)
	archive ${RF_MSVCVERSION} ${ARCHIVE_NAME} ${APP_NAME} >> ${RF_OBJDIR}/install.log 2>&1
;;
esac

if [ "${APP_NAME}" = "mozilla" ] ; then
	archive ${RF_MSVCVERSION} gecko-sdk-${RF_VERSION}-${RF_BUILDNAME} gecko-sdk >> ${RF_OBJDIR}/install.log 2>&1
fi

if [ "${APP_NAME}" = "xulrunner" ] ; then
	${RF_MAKE_CMD} -C ${RF_OBJDIR}/${RF_INSTALLER} make-sdk >> ${RF_OBJDIR}/install.log 2>&1
	ERROR=$?
	if [ ${ERROR} -ne 0 ] ; then
		echo ${RF_APP_VER}_${RF_MSVCVERSION} >> ${BUILDROOT}/failed.log
		echo ${RF_APP_VER}_${RF_MSVCVERSION} >> ${BUILDROOT}/${RF_APP_VER}/failed_${RF_MSVCVERSION}.log
		exit
	fi
	archive ${RF_MSVCVERSION} xulrunner-sdk-${RF_VERSION}-${RF_BUILDNAME} xulrunner-sdk >> ${RF_OBJDIR}/install.log 2>&1
fi

case ${RF_MSVCVERSION} in
win32-*|win64-*)
	cd ${APP_NAME}
	for EXE in `ls *.exe` ; do
		mt -inputresource:${EXE} -out:..\\${EXE}.manifest.xml > /dev/null 2>&1
	done
	cd ${OLDPWD}
	if [ -d ${APP_NAME}/uninstall ] ; then
		cd ${APP_NAME}/uninstall
		for EXE in `ls *.exe` ; do
			mt -inputresource:${EXE} -out:..\\..\\${EXE}.manifest.xml > /dev/null 2>&1
		done
		cd ${OLDPWD}
	fi
;;
esac

if [ "${PROFILE_GEN_SCRIPT}" ] ; then
	cd ${BUILDROOT}/${RF_APP_VER}/${RF_SRCDIR}
	MOZ_PGO_INSTRUMENTED=1 OBJDIR=${RF_OBJDIR} JARLOG_DIR=${RF_OBJDIR}/jarlog/en-US.log ${PROFILE_GEN_SCRIPT} > ${RF_OBJDIR}/pgotest.log 2>&1
	ERROR=$?
	if [ ${ERROR} -ne 0 ] ; then
		echo ${RF_APP_VER}_${RF_MSVCVERSION}_pgo_build_broken >> ${BUILDROOT}/failed.log
		cp ${RF_OBJDIR}/pgotest.log ${BUILDROOT}/${RF_APP_VER}/build_broken_${RF_MSVCVERSION}.log
	fi
	cd ${OLDPWD}
fi

touch ${RF_OBJDIR}/build.done
if [ ! "${RF_DONTDIST}" ] && [ "${RF_ALLOWDIST}" ] ; then
	mkdir -p ${SRCDIR}/${RF_APP_VER}
	cp -p *-${RF_BUILDNAME}.* ${SRCDIR}/${RF_APP_VER} > /dev/null 2>&1
	rm -f ${SRCDIR}/${RF_APP_VER}/._*
fi
