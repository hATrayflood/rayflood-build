#!/bin/bash

#APPS="firefox-24.6.0esr thunderbird-24.6.0 xulrunner-24.6.0esr:firefox-24.6.0esr seamonkey-2.21.6.0esr:thunderbird-24.6.0"
#APPS="firefox-17.0.9esr thunderbird-17.0.9esr xulrunner-17.0.9esr:firefox-17.0.9esr seamonkey-2.14.9esr:thunderbird-17.0.9esr fennec-17.0.9esr:firefox-17.0.9esr lightning-sunbird-1.9.9esr:thunderbird-17.0.9esr"
#APPS="firefox-10.0.12esr thunderbird-10.0.12esr xulrunner-10.0.12esr:firefox-10.0.12esr seamonkey-2.7.12esr:thunderbird-10.0.12esr fennec-10.0.12esr:firefox-10.0.12esr lightning-sunbird-1.2.12esr:thunderbird-10.0.12esr"
#APPS="firefox-3.6.14 thunderbird-3.1.8  xulrunner-1.9.2.14:firefox-3.6.14 lightning-1.0b2.8:thunderbird-3.1.8"
#APPS="firefox-3.5.17 thunderbird-3.0.11 xulrunner-1.9.1.17:firefox-3.5.17  sunbird-1.0b1.11:thunderbird-3.0.11 seamonkey-2.0.12"
#APPS="firefox-3.0.19 xulrunner-1.9.0.19:firefox-3.0.19 lightning-1.0b2 sunbird-1.0b1"
#APPS="firefox-2.0.0.20 thunderbird-2.0.0.24 seamonkey-1.1.19 lightning-sunbird-0.9"
#APPS="firefox-1.5.0.12 thunderbird-1.5.0.14 seamonkey-1.0.9"
#APPS="firefox-1.0.8 thunderbird-1.0.8 mozilla-1.7.13"

APPS="firefox-24.8.1esr thunderbird-24.8.1 xulrunner-24.8.1esr:firefox-24.8.1esr seamonkey-2.21.8.1esr:thunderbird-24.8.1"
