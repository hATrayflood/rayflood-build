#!/bin/bash

APP_NAME=${1}
SRC_DIR=${2}
SUB_DIR=${3}
OBJ_DIR=${4}
MZ_CONF=${SRC_DIR}/.mozconfig

cat << EOF > ${MZ_CONF}
mk_add_options MOZ_OBJDIR=${OBJ_DIR}
mk_add_options MOZ_MAKE_FLAGS=-j1
ac_add_options --enable-optimize
ac_add_options --enable-jemalloc
ac_add_options --disable-tests
ac_add_options --disable-debug
ac_add_options --disable-debug-symbols
EOF

if echo ${RF_MSVCVERSION} | grep -sq ^win64- ; then
	echo "ac_add_options  --build=x86_64-pc-mingw32" >> ${MZ_CONF}
	echo "ac_add_options   --host=x86_64-pc-mingw32" >> ${MZ_CONF}
	echo "ac_add_options --target=x86_64-pc-mingw32" >> ${MZ_CONF}
fi

case "${APP_NAME}" in
firefox)
	BUILD_APP=browser
	echo "ac_add_options --enable-metro" >> ${MZ_CONF}
;;
xulrunner)
	BUILD_APP=xulrunner
;;
fennec)
	BUILD_APP=mobile/xul
;;
b2g)
	BUILD_APP=b2g
;;
thunderbird)
	BUILD_APP=mail
	echo "ac_add_options --enable-calendar" >> ${MZ_CONF}
;;
seamonkey)
	BUILD_APP=suite
;;
sunbird)
	BUILD_APP=calendar
	echo "ac_add_options --enable-calendar" >> ${MZ_CONF}
;;
esac
echo "ac_add_options --enable-application=${BUILD_APP}" >> ${MZ_CONF}
