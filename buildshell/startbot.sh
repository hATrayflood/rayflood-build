#!/bin/bash

mkdir -p ${RF_TOOLKIT}/../mozilla
BUILDROOT=$(cd ${RF_TOOLKIT}/../mozilla && pwd)
RT_DIR=${BUILDROOT}/central
MC_DIR=${BUILDROOT}/central/mozilla-central
CC_DIR=${BUILDROOT}/central/comm-central
CM_DIR=${BUILDROOT}/central/comm-central/mozilla
LD_DIR=${BUILDROOT}/central/comm-central/ldap/sdks
PM_DIR=${BUILDROOT}/pymake
DIST_DIR=/t/central
SHELLDIR=${RF_TOOLKIT}/buildshell
. ${RF_TOOLKIT}/mozconfig/setsdk81dir.sh

archive(){
	rm -f ${2}.7z
	rm -f ${2}.tar.xz
	case ${1} in
	win*)
		7z a -mmt1 ${2}.7z ${3}
	;;
	mac*)
		tar cvf ${2}.tar ${3}
		xz ${2}.tar
	;;
	*)
		tar Jcvf ${2}.tar.xz ${3}
	;;
	esac
}

rm_psutil_build(){
	rm -rf ${1}/python/psutil/*.so
	rm -rf ${1}/python/psutil/*.pyd
	rm -rf ${1}/python/psutil/build/
}

get_rev(){
	hg summary | grep ^parent: | awk '{print $2}' | sed -e 's/:/_/'
}

get_mc_rev(){
	echo $(cd ${MC_DIR} && get_rev)
}

get_cc_rev(){
	echo $(cd ${CC_DIR} && get_rev)-$(cd ${CM_DIR} && get_rev)
}

update_repo(){
	hg pull
	hg update
}

mc_update(){
	(cd ${MC_DIR} && update_repo)
	(cd ${PM_DIR} && update_repo)
}

cc_update(){
	(cd ${CC_DIR} && update_repo)
	(cd ${CM_DIR} && update_repo)
	(cd ${LD_DIR} && update_repo)
	(cd ${PM_DIR} && update_repo)

	(cd ${CM_DIR}/extensions/inspector && update_repo)
	(cd ${CM_DIR}/extensions/irc       && update_repo)
	(cd ${CM_DIR}/extensions/venkman   && update_repo)
}

build(){
	APP_NAME=${1}
	case "${APP_NAME}" in
	firefox|xulrunner|fennec|b2g)
		SRC_DIR=${MC_DIR}
		SUB_DIR=.
		SRC_REV=`get_mc_rev`
	;;
	thunderbird|seamonkey|sunbird)
		SRC_DIR=${CC_DIR}
		SUB_DIR=mozilla
		SRC_REV=`get_cc_rev`
	;;
	*)
		echo "invalid app \"${APP_NAME}\"."
		return 1
	;;
	esac

	OBJ_DIR=../${APP_NAME}-${RF_MSVCVERSION}-${SRC_REV}
	if [ -f ${OBJ_DIR}/build.done ] ; then
		return 0
	fi

	echo "start ${APP_NAME}-${RF_MSVCVERSION}-${SRC_REV}"
	rm_psutil_build ${SRC_DIR}/${SUB_DIR}
	cd ${SRC_DIR}
	mkdir -p ${OBJ_DIR}
	. ${SHELLDIR}/central-config.sh ${APP_NAME} ${SRC_DIR} ${SUB_DIR} ${OBJ_DIR}

	if [ -e ${OBJ_DIR}/build.log ] ; then
		TAKE=0
		while [ -e ${OBJ_DIR}/build.log.${TAKE} ] ; do
			TAKE=$((${TAKE} + 1))
		done
		mv ${OBJ_DIR}/build.log ${OBJ_DIR}/build.log.${TAKE}
	fi

	touch ${DIST_DIR}/${APP_NAME}-${RF_MSVCVERSION}-${SRC_REV}.build.now
	env | sort >  ${OBJ_DIR}/env.txt
	pymake -f client.mk build > ${OBJ_DIR}/build.log 2>&1
	ERROR=$?
	rm ${DIST_DIR}/${APP_NAME}-${RF_MSVCVERSION}-${SRC_REV}.build.now
	if [ ${ERROR} -ne 0 ] ; then
		cd ${OBJ_DIR}
		archive ${RF_MSVCVERSION} ${APP_NAME}-${RF_MSVCVERSION}-${SRC_REV}.build.log build.log >> ${OBJ_DIR} 2>&1
		rm -f ${DIST_DIR}/${APP_NAME}-${RF_MSVCVERSION}-*.build.log.7z
		rm -f ${DIST_DIR}/${APP_NAME}-${RF_MSVCVERSION}-*.build.log.tar.xz
		cp -p ${APP_NAME}-${RF_MSVCVERSION}-${SRC_REV}.build.log.7z     ${DIST_DIR} >> /dev/null 2>&1
		cp -p ${APP_NAME}-${RF_MSVCVERSION}-${SRC_REV}.build.log.tar.xz ${DIST_DIR} >> /dev/null 2>&1
		rm -f send_message.txt
		echo ${SRC_DIR}/.mozconfig: >> send_message.txt
		cat  ${SRC_DIR}/.mozconfig  >> send_message.txt
		echo "" >> send_message.txt
		echo env:    >> send_message.txt
		cat  env.txt >> send_message.txt
		python ${SHELLDIR}/sendmail.py ${RF_SMTP} `hostname` ${RF_SENDTO} "[BUILD FAILD] ${APP_NAME}-${RF_MSVCVERSION}-${SRC_REV}" send_message.txt
		return ${ERROR}
	fi

	cp -p ${RT_DIR}/msvcr100.dll ${OBJ_DIR}/${SUB_DIR}/dist/bin
	cp -p ${RT_DIR}/msvcp100.dll ${OBJ_DIR}/${SUB_DIR}/dist/bin
	cd ${OBJ_DIR}
	OBJ_DIR=`pwd`
	touch build.done
	pymake -C ${BUILD_APP}/installer > install.log 2>&1
	if [ "${APP_NAME}" = "xulrunner" ] ; then
		pymake -C ${BUILD_APP}/installer make-sdk >> install.log 2>&1
	fi
	cd ${SUB_DIR}/dist
	cp -p ${RT_DIR}/msvcr100.dll ${APP_NAME}
	cp -p ${RT_DIR}/msvcp100.dll ${APP_NAME}
	archive ${RF_MSVCVERSION} ${APP_NAME}-${RF_MSVCVERSION}-${SRC_REV} ${APP_NAME} >> ${OBJ_DIR} 2>&1
	rm -f ${DIST_DIR}/${APP_NAME}-${RF_MSVCVERSION}-*.*
	cp -p ${APP_NAME}-${RF_MSVCVERSION}-${SRC_REV}.* ${DIST_DIR} > /dev/null 2>&1
	if [ -f xpi-stage/lightning*.xpi ] ; then
		rm -f ${DIST_DIR}/lightning-${RF_MSVCVERSION}-*.xpi
		cp -p `ls xpi-stage/lightning*.xpi` ${DIST_DIR}/lightning-${RF_MSVCVERSION}-${SRC_REV}.xpi > /dev/null 2>&1
	fi
	if [ -d xulrunner-sdk ] ; then
		archive ${RF_MSVCVERSION} xulrunner-sdk-${RF_MSVCVERSION}-${SRC_REV} xulrunner-sdk >> ${OBJ_DIR} 2>&1
		rm -f ${DIST_DIR}/xulrunner-sdk-${RF_MSVCVERSION}-*.*
		cp -p xulrunner-sdk-${RF_MSVCVERSION}-${SRC_REV}.* ${DIST_DIR} > /dev/null 2>&1
	fi

	cd ${RT_DIR}
	for OLD_OBJ_DIR in `ls -d ${APP_NAME}-${RF_MSVCVERSION}-*` ; do
		if ! echo ${OLD_OBJ_DIR} | grep -sq -- -${SRC_REV}$ ; then
			rm -rf ${OLD_OBJ_DIR}
		fi
	done
}

bot(){
	if [ -z "${OLD_MC_REV}" ] || [ -z "${OLD_CC_REV}" ] ; then
		mc_update
		cc_update
	fi
	CUR_MC_REV=`get_mc_rev`
	CUR_CC_REV=`get_cc_rev`

	echo ""
	build firefox
	sleep 5m
	build xulrunner
	sleep 5m
	OLD_MC_REV=${CUR_MC_REV}
	build thunderbird
	sleep 5m
	build seamonkey
	sleep 5m
	#build sunbird
	#sleep 5m
	OLD_CC_REV=${CUR_CC_REV}

	mc_update
	cc_update
	CUR_MC_REV=`get_mc_rev`
	CUR_CC_REV=`get_cc_rev`
	while [ "${OLD_MC_REV}" = "${CUR_MC_REV}" ] && [ "${OLD_CC_REV}" = "${CUR_CC_REV}" ] ; do
		sleep 1h
		echo ""
		mc_update
		cc_update
		CUR_MC_REV=`get_mc_rev`
		CUR_CC_REV=`get_cc_rev`
	done
}

if [ "${1}" ] ; then
	build ${1}
	exit
fi

while : ; do
	bot
done
