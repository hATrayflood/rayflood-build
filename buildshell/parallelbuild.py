#!/usr/bin/env python

from __future__ import with_statement

import os
import sys
import re
import time
import datetime
import threading
import subprocess

def build(thn, pgo):
	(app, cc) = ("dummy", "dummy")
	while app:
		(app, cc) = ("", "")
		for i in range(len(q)):
			(_app, _cc) = q[i]
			if pgo and not isPgo(_app, _cc):
				continue
			if not appm[_app]:
				(app, cc) = (_app, _cc)
				appm[app] = cc
				q.pop(i)
				break

		t[thn]["pgo"] = pgo
		t[thn]["app"] = app
		t[thn]["cc"]  = cc
		if app:
			cmd = [batpref + "-" + cc + batsuff, buildsh, app, str(rfthreads)]
			t[thn]["cmd"]   = cmd
			t[thn]["start"] = datetime.datetime.now()
			for i in range(retry):
				t[thn]["take"] = i
				print("Thread-" + str(thn) + ": take " + str(i) + ", pgo = " + str(pgo) + ", cc = " + cc + ", app = " + app)
				with wait_lock:
					time.sleep(1)
				subprocess.call(cmd, stdout=open(os.devnull, mode="w"))
				if os.path.exists(getBuildBrokenLog(app, cc)):
					print("**BUILD BROKEN** Thread-" + str(thn) + ": pgo = " + str(pgo) + ", cc = " + cc + ", app = " + app)
					break
				if not os.path.exists(getFailedLog(app, cc)):
					break
			appm[app] = ""
		else:
			if pgo:
				build(thn, False)

def getParams(appsfile, paramname):
	params = []
	with open(appsfile) as file:
		for line in file:
			if not (line.isspace() or line.lstrip().startswith("#")):
				name, value = line.strip().split("=", 1)
				if name == paramname:
					params = value.strip("\"'").split()
	return params

def getFailedLog(app, cc):
	return buildroot + "/" + app.split(':')[0] + "/failed_" + cc + ".log"

def getBuildBrokenLog(app, cc):
	return buildroot + "/" + app.split(':')[0] + "/build_broken_" + cc + ".log"

def checkconf(app, cc):
	key = app + " " + cc
	if not key in checkconf_cache:
		checkconfcmd = bash.split()
		env = os.environ.copy()
		env['PATH'] = env['PATH'] + os.pathsep + os.path.dirname(checkconfcmd[-1])
		checkconfcmd.append(checkconfsh)
		checkconfcmd.append(app)
		checkconfcmd.append(cc)
		checkconf_cache[key] = subprocess.Popen(checkconfcmd, env=env, stdout=subprocess.PIPE).communicate()
	return checkconf_cache[key]

def isPgo(app, cc):
	(stdout, stderr) = checkconf(app, cc)
	if stdout.find('RF_PGO') > -1:
		return True
	return False

def dontBuild(app, cc):
	(stdout, stderr) = checkconf(app, cc)
	if stdout.find('RF_DONTBUILD') > -1:
		return True
	return False

if __name__ == "__main__":
	if len(sys.argv) <= 6:
		print "syntax: %PYTHONDIR%\python parallelbuild.py THREADS PGO_THREADS %RF_SHELLDIR%\bin\bash /s/rayflood-build/buildshell S:\rayflood-build\buildshell\apps.sh S:\rayflood-build\rayflood vc6 gcc3"
		sys.exit()

	retry = 4
	checkconf_cache = {}
	wait_lock = threading.Lock()

	threads = int(sys.argv[1])
	pgothreads = int(sys.argv[2])
	bash = sys.argv[3]
	buildshell = sys.argv[4]
	checkconfsh = buildshell + "/checkconf.sh"
	preparesh = buildshell + "/prepareall.sh"
	buildsh = buildshell + "/build.sh"
	appsfile = sys.argv[5]
	apps = getParams(appsfile, "APPS")
	buildroot = os.path.abspath(appsfile + "/../../../mozilla")
	batpref = sys.argv[6]
	batsuff = ".bat"
	ccs = sys.argv[7:]

	if threads <= len(apps):
		rfthreads = 1
	else:
		rfthreads = threads / len(apps)
		threads = threads / rfthreads
	if pgothreads > threads:
		pgothreads = threads

	print("start build with " + str(threads) + " thread(s) (" + str(pgothreads) + " pgo thread(s) ).")
	print("app = " + str(apps))
	print("cc  = " + str(ccs))
	print("")
	q = []
	appm = {}
	for cc in ccs:
		for app in apps:
			appm[app] = ""
			if dontBuild(app, cc):
				print("skip\t" + cc + "\t" + app)
				continue
			q.append((app, cc))
			if isPgo(app, cc):
				print("PGO\t" + cc + "\t" + app)
	print("")

	print("preparing ...")
	subprocess.call([batpref + "-" + ccs[0] + batsuff, preparesh], stdout=open(os.devnull, mode="w"))

	t = []
	for i in range(threads):
		t.append({})
		time.sleep(1)
		t[i]["target"] = threading.Thread(target=build, args=(i, bool(i < pgothreads)))
		t[i]["target"].start()
	for i in range(threads):
		t[i]["target"].join()
