#!/bin/bash

SHELLDIR=${RF_TOOLKIT}/buildshell

mkdir -p ${RF_TOOLKIT}/../mozilla
BUILDROOT=$(cd ${RF_TOOLKIT}/../mozilla && pwd)
SRCDIR=/b/mozilla
#SRCDIR=/tmp
if [ ! -e ${SRCDIR} ] ; then
	mkdir -p ${RF_TOOLKIT}/../mozilla-release
	SRCDIR=$(cd ${RF_TOOLKIT}/../mozilla-release && pwd)
fi
PTCDIR=${RF_TOOLKIT}/patches
CONFIG_SHELL=/bin/bash
RF_MOZCONFIG=${RF_TOOLKIT}/mozconfig

mk_add_options(){
	echo "mk_add_options ${1}"
	#return
}
ac_add_options(){
	echo "ac_add_options ${1}"
	#return
}
ac_add_app_options(){
	echo "ac_add_app_options ${1}"
	#return
}

archive(){
	rm -f ${2}.7z
	rm -f ${2}.tar.xz
	case ${1} in
	win*)
		7z a -mmt1 ${2}.7z ${3}
	;;
	mac*)
		tar cvf ${2}.tar ${3}
		xz ${2}.tar
	;;
	*)
		tar Jcvf ${2}.tar.xz ${3}
	;;
	esac
}
