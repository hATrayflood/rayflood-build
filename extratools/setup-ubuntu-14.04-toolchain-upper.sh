#!/bin/sh
# ubuntu-12.04

add-apt-repository -y ppa:h-rayflood/gcc-lower
add-apt-repository -y ppa:h-rayflood/gcc-upper
add-apt-repository -y ppa:h-rayflood/llvm
apt-get update
apt-get -y dist-upgrade

apt-get -y install libcloog-ppl0
apt-get -y install libcloog-ppl1
apt-get -y install libcloog-isl3
apt-get -y install libcloog-isl4
apt-get -y install g++
apt-get -y install g++-3.3
apt-get -y install g++-4.4
apt-get -y install g++-4.5
apt-get -y install g++-4.6
apt-get -y install g++-4.7
apt-get -y install g++-4.8
apt-get -y install g++-4.9
apt-get -y install clang-2.9
apt-get -y install clang-3.0
apt-get -y install clang-3.1
apt-get -y install clang-3.2
apt-get -y install clang-3.3
apt-get -y install clang-3.4
apt-get -y install clang-3.5
apt-get -y install clang-3.6
