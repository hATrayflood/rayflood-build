#!/bin/sh

sudo sh << EOC
`dirname $0`/setup-ubuntu-`lsb_release -r | awk '{print $2}'`.sh
if [ "${RF_UPPER_TOOLCHAIN}" ] ; then
`dirname $0`/setup-ubuntu-`lsb_release -r | awk '{print $2}'`-toolchain-upper.sh
else
`dirname $0`/setup-ubuntu-`lsb_release -r | awk '{print $2}'`-toolchain.sh
fi
EOC
