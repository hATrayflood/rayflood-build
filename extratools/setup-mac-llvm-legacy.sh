#!/bin/sh

RF_DEFAULT_VARIANTS="+llvm29 -arm_runtime"
RF_MACPORTS_PREFIX=/opt/llvm-legacy
RF_PORTS_SOURCES=http://rayflood.org/mozilla/ports-130866.tar
. `dirname ${0}`/setup-macports.sh ${RF_MACPORTS_PREFIX}

if [ "${RF_AFTER_MARVERICKS}" ] ; then
echo legacy llvm not supported after mavericks.
exit 1
fi

sudo sh << EOC
cd ${RF_MACPORTS_PREFIX}/bin
./port sync
./port install clang-2.9
./port install clang-3.0
#./port install dragonegg-3.0
./port install clang-3.1
#./port install dragonegg-3.1
./port install clang-3.2
#./port install dragonegg-3.2
EOC
