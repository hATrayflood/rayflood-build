#!/bin/sh
# ubuntu-10.04

apt-get -y install patch
apt-get -y install xz-utils
apt-get -y install cvs
apt-get -y install subversion
apt-get -y install mercurial
apt-get -y install git-core
apt-get -y install yasm-1
apt-get -y install autoconf2.13
apt-get -y install flashplugin-installer

apt-get -y install libgnomevfs2-dev
apt-get -y install libnotify-dev
apt-get -y install libiw-dev
apt-get -y install libasound2-dev
apt-get -y install libcurl4-gnutls-dev
apt-get -y install mesa-common-dev
apt-get -y install python-dev
apt-get -y install default-jdk
apt-get -y install libgstreamer-plugins-base0.10-dev

sudo -u ${SUDO_USER} `dirname ${0}`/setup-python27.sh
