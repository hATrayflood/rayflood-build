#!/bin/sh

RF_MACPORTS_VERSION=2.3.4
RF_MACPORTS_PREFIX=${1}
RF_MACPORTS_CONFIGURE="./configure --prefix=${RF_MACPORTS_PREFIX} --with-applications-dir=${RF_MACPORTS_PREFIX}/Applications"

RF_DARWIN_VERSION=`uname -r | awk -F . '{print $1}'`
if [ ${RF_DARWIN_VERSION} -ge 13 ] ; then
RF_AFTER_MARVERICKS=1
fi
if [ ${RF_DARWIN_VERSION} -ge 14 ] ; then
RF_AFTER_YOSEMITE=1
fi

if [ -z "${RF_MACPORTS_PREFIX}" ] ; then
echo `dirname ${0}`/setup-mac.sh or `dirname ${0}`/setup-mac-llvm.sh
exit 1
fi

JAVA_HOME=`/usr/libexec/java_home -v 1.6 2>/dev/null`
if [ "${JAVA_HOME}" ] ; then
export PATH="${JAVA_HOME}/bin:${PATH}"
fi
if ! javac -version > /dev/null 2>&1 ; then
echo javac not found. please install Java for OS X.
exit 1
fi

RF_XCODE_VERSION=`xcodebuild -version | grep ^Xcode | awk '{print $2}'`
if [ -z "${RF_XCODE_VERSION}" ] ; then
echo xcode not found. please install Xcode.
exit 1
fi

if [ `echo ${RF_XCODE_VERSION} | awk -F. '{print $1}'` -eq 3 ] ; then
RF_UNIVERSAL_ARCHS="x86_64 i386 ppc"
fi
if [ `echo ${RF_XCODE_VERSION} | awk -F. '{print $1}'` -eq 4 ] ; then
if [ `echo ${RF_XCODE_VERSION} | awk -F. '{print $2}'` -eq 3 ] ; then
RF_CLTOOLS_PKGNAME=com.apple.pkg.DeveloperToolsCLI
fi
fi
if [ `echo ${RF_XCODE_VERSION} | awk -F. '{print $1}'` -ge 5 ] ; then
RF_CLTOOLS_PKGNAME=com.apple.pkg.DeveloperToolsCLI
fi
if [ "${RF_AFTER_MARVERICKS}" ] ; then
RF_CLTOOLS_PKGNAME=com.apple.pkg.CLTools_Executables
fi

if [ "${RF_CLTOOLS_PKGNAME}" ] ; then
if ! pkgutil --pkg-info ${RF_CLTOOLS_PKGNAME} > /dev/null 2>&1 ; then
echo compiler not found. please install Command Line Tools.
echo view command_line_tools.url to download and install.
echo or run \'xcode-select --install\'
exit 1
fi
fi

for CC in clang llvm-gcc-4.2 gcc-4.2 ; do
if ${CC} --version  > /dev/null 2>&1 ; then
RF_DEFAULT_COMPILER=${CC}
fi
done
if [ -z "${RF_DEFAULT_COMPILER}" ] ; then
echo compiler not found. please install Command Line Tools.
echo view command_line_tools.url to download and install.
exit 1
fi

if [ ${RF_DEFAULT_COMPILER} != gcc-4.2 ] ; then
RF_FORCE_COMPILER=
fi
if [ "${RF_FORCE_COMPILER}" ] ; then
RF_MACPORTS_CONFIGURE="${RF_MACPORTS_CONFIGURE} CC=${RF_DEFAULT_COMPILER}"
fi

cd `dirname ${0}`/../..
RF_OSX_VERSION=`sw_vers -productVersion`
RF_MACPORTS_BUILD=`pwd`/macports_build/osx-`echo ${RF_OSX_VERSION} | awk -F. '{print $1"."$2}'`_xcode-`echo ${RF_XCODE_VERSION} | awk -F. '{print $1"."$2}'`
if [ ! -f MacPorts-${RF_MACPORTS_VERSION}.tar.bz2 ] ; then
#curl -k -O https://distfiles.macports.org/MacPorts/MacPorts-${RF_MACPORTS_VERSION}.tar.bz2
curl -k -O http://jaist.dl.sourceforge.net/project/macports/MacPorts/${RF_MACPORTS_VERSION}/MacPorts-${RF_MACPORTS_VERSION}.tar.bz2
fi
if [ -e MacPorts-${RF_MACPORTS_VERSION} ] ; then
rm -rf MacPorts-${RF_MACPORTS_VERSION}
fi
tar jxf MacPorts-${RF_MACPORTS_VERSION}.tar.bz2
cd MacPorts-${RF_MACPORTS_VERSION}

if [ "${RF_UNIVERSAL_ARCHS}" ] ; then
${RF_MACPORTS_CONFIGURE} --with-universal-archs="${RF_UNIVERSAL_ARCHS}"
else
${RF_MACPORTS_CONFIGURE}
fi
if ! make ; then
exit 1
fi

sudo sh << EOC
make install
rm -rf ${RF_MACPORTS_PREFIX}/var/macports/build
mkdir -p ${RF_MACPORTS_BUILD}
ln -s ${RF_MACPORTS_BUILD} ${RF_MACPORTS_PREFIX}/var/macports/build
if [ "${RF_FORCE_COMPILER}" ] ; then
echo "default_compilers ${RF_DEFAULT_COMPILER}" >> ${RF_MACPORTS_PREFIX}/etc/macports/macports.conf
fi
if [ "${RF_DEFAULT_VARIANTS}" ] ; then
echo "${RF_DEFAULT_VARIANTS}" >> ${RF_MACPORTS_PREFIX}/etc/macports/variants.conf
fi
if [ "${RF_PORTS_SOURCES}" ] ; then
sed -i -e "s|^rsync://rsync.macports.org/release/tarballs/ports.tar|${RF_PORTS_SOURCES}|g" ${RF_MACPORTS_PREFIX}/etc/macports/sources.conf
fi
EOC

if ! sudo -u macports xcodebuild -version > /dev/null 2>&1 ; then
if ! sudo xcodebuild -license ; then
sudo rm -rf ${RF_MACPORTS_PREFIX}
exit 1
fi
fi
