#!/bin/bash

cd /tmp
REV=130866
rm -rf ports-${REV}.tar ports
svn export -r ${REV} http://svn.macports.org/repository/macports/trunk/dports ports
/opt/llvm-mp/bin/portindex ports
tar cf ports-${REV}.tar ports
