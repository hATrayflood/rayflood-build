#!/bin/sh
# ubuntu-10.04

add-apt-repository ppa:h-rayflood/gcc-lower
add-apt-repository ppa:h-rayflood/llvm
apt-get update
apt-get -y dist-upgrade

apt-get -y install libcloog-ppl0
apt-get -y install g++
apt-get -y install g++-3.3
apt-get -y install g++-3.4
apt-get -y install g++-4.0
apt-get -y install g++-4.1
apt-get -y install g++-4.2
apt-get -y install g++-4.3
apt-get -y install g++-4.4
apt-get -y install llvm-gcc-4.2
apt-get -y install clang
apt-get -y install clang-2.9
apt-get -y install clang-3.0
apt-get -y install clang-3.1
apt-get -y install clang-3.2
