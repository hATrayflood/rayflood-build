call %RF_TOOLKIT%\extratools\gcc-mingw\mingw-gcc45\mingw32.bat

cd /d %RF_TOOLKIT%\..\gcc-mingw\clang29
set PATH=%CD%\bin;%PATH%

set  CC=clang
set CXX=clang++
set CPP=%CC% -E
set CXXCPP=%CXX% -E

cd /d %RF_TOOLKIT%\..\gcc-mingw\mingw-gcc45\lib\gcc\mingw32\4.5.2
set RF_LIBGCC=%CD%
set CFLAGS=-I%RF_MINGWDIR%\include -I%RF_MINGWDIR%\include\gdiplus
set CXXFLAGS=%CFLAGS% -I%RF_LIBGCC%\include\c++\mingw32 -I%RF_LIBGCC%\include\c++
