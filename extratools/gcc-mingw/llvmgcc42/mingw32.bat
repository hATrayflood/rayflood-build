call %RF_TOOLKIT%\extratools\gcc-mingw\mingw\mingw32.bat

cd /d %RF_TOOLKIT%\..\gcc-mingw\llvmgcc42
set PATH=%CD%\bin;%PATH%

set  CC=llvm-gcc
set CXX=llvm-g++
set CPP=%CC% -E
set CXXCPP=%CXX% -E
set LOCAL_INCLUDES=-I%CD%\include\c++\4.2.1
