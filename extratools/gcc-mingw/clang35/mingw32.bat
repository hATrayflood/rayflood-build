call %RF_TOOLKIT%\extratools\gcc-mingw\mingw-gcc45\mingw32.bat

cd /d %RF_TOOLKIT%\..\gcc-mingw\clang35
set PATH=%CD%\bin;%PATH%

set  CC=clang
set CXX=clang++
set CPP=%CC% -E
set CXXCPP=%CXX% -E

cd /d %RF_TOOLKIT%\..\gcc-mingw\mingw-gcc45\lib\gcc\mingw32\4.5.2
set C_INCLUDE_PATH=%C_INCLUDE_PATH%;%CD%\include\c++\mingw32;%CD%\include\c++
set CPLUS_INCLUDE_PATH=%C_INCLUDE_PATH%
