cd /d %RF_TOOLKIT%\..\gcc-mingw\mingw
set RF_MINGWDIR=%CD%
set PATH=%RF_MINGWDIR%\bin;%PATH%

set C_INCLUDE_PATH=%RF_MINGWDIR%\include;%RF_MINGWDIR%\include\gdiplus
set CPLUS_INCLUDE_PATH=%C_INCLUDE_PATH%
set LIBRARY_PATH=%RF_MINGWDIR%\lib

set LD=ld
set AS=as
