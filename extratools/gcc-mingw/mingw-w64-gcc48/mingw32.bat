cd /d %RF_TOOLKIT%\..\gcc-mingw\mingw-w64-gcc48\mingw32
set PATH=%CD%\bin;%PATH%

set  CC=gcc
set CXX=g++
set CPP=%CC% -E
set CXXCPP=%CXX% -E
set LDFLAGS=-static-libgcc -static-libstdc++
set DLLFLAGS=%LDFLAGS%
set LD=ld
set AS=as

set C_INCLUDE_PATH=%CD%\i686-w64-mingw32\include\gdiplus
set CPLUS_INCLUDE_PATH=%C_INCLUDE_PATH%
