call %RF_TOOLKIT%\extratools\gcc-mingw\mingw-w64-gcc46\mingw64.bat

cd /d %RF_TOOLKIT%\..\gcc-mingw\mingw-w64-clang32\mingw64
set PATH=%CD%\bin;%PATH%

set  CC=clang
set CXX=clang++
set CPP=%CC% -E
set CXXCPP=%CXX% -E

cd /d %RF_TOOLKIT%\..\gcc-mingw\mingw-w64-gcc46\mingw64
set C_INCLUDE_PATH=%C_INCLUDE_PATH%;%CD%\x86_64-w64-mingw32\include;%CD%\include\c++\4.6.3\x86_64-w64-mingw32;%CD%\include\c++\4.6.3
set CPLUS_INCLUDE_PATH=%C_INCLUDE_PATH%
