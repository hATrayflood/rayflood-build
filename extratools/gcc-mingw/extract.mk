SFMIRROR ?= http://heanet.dl.sourceforge.net/project/

ifneq ($(SF),)
MIRROR = $(SFMIRROR)
endif
ifdef MIRROR
URLS := $(addprefix $(MIRROR),$(URLS))
endif

DSTDIR   = $(shell cd $(CURDIR)/../../../.. && pwd)/$(shell echo $(CURDIR) | awk -F / '{ print $$(NF-1) }')/$(shell echo $(CURDIR) | awk -F / '{ print $$(NF) }')/
FILES    = $(notdir $(URLS))
DSTFILES = $(addprefix $(DSTDIR),$(FILES))

all: clean $(FILES)

clean:
	rm -rf $(addprefix $(DSTDIR),$(filter-out $(FILES) . ..,$(shell ls -a $(DSTDIR))))

distclean:
	rm -rf $(DSTDIR)

$(DSTDIR):
	mkdir -p $@

$(DSTFILES): $(DSTDIR)
	cd $(DSTDIR) && wget -nc $(filter %/$(notdir $@),$(URLS))

$(FILES): $(DSTFILES)

%.tar.gz:
	cd $(DSTDIR) && tar  zxf $@

%.tar.bz2:
	cd $(DSTDIR) && tar  jxf $@

%.tar.lzma:
	cd $(DSTDIR) && lzma -cd $@ | tar xf -

%.tar.xz:
	cd $(DSTDIR) && xz   -cd $@ | tar xf -

%.zip:
	cd $(DSTDIR) && unzip $@ > /dev/null

%.7z:
	cd $(DSTDIR) && 7z x $@ -y > /dev/null
