#!/bin/sh

RF_EXTRA_DIR=$(cd `dirname ${0}` && pwd)
RF_FORCE_COMPILER=1
RF_DEFAULT_VARIANTS="+universal"
RF_MACPORTS_PREFIX=/opt/moztools
. `dirname ${0}`/setup-macports.sh ${RF_MACPORTS_PREFIX}

sudo sh << EOC
cd ${RF_MACPORTS_PREFIX}/bin
./port sync
./port install xz
./port install libidl
./port install autoconf213
./port install yasm
EOC

# under python-2.7.3
if which python2.7 > /dev/null ; then
if [ `python2.7 -c "import sys; print sys.hexversion"` -lt 34014192 ] ; then
${RF_EXTRA_DIR}/setup-python27.sh
fi
fi
