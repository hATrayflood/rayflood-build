#!/bin/sh

RF_DEFAULT_VARIANTS="+llvm33 -arm_runtime +ld64_236"
RF_MACPORTS_PREFIX=/opt/llvm-mp
. `dirname ${0}`/setup-macports.sh ${RF_MACPORTS_PREFIX}

sudo sh << EOC
cd ${RF_MACPORTS_PREFIX}/bin
./port sync
#./port install gcc43
#./port install gcc44
#./port install gcc45
#./port install gcc46
#./port install gcc47
#./port install gcc48
#./port install gcc49
#./port install gcc5
if [ -z "${RF_AFTER_YOSEMITE}" ] ; then
if ! which gcc-4.2 > /dev/null ; then
./port install apple-gcc42
fi
if ! which llvm-gcc-4.2 > /dev/null ; then
./port install llvm-gcc42
fi
fi

./port install clang-3.3
#./port install dragonegg-3.3
./port install clang-3.4
#./port install dragonegg-3.4
./port install clang-3.5
./port install clang-3.6
./port install clang-3.7
./port install clang-3.8
EOC
