#!/bin/bash

cp -p etc/profile.d/profile-rfpaths.sh /etc/profile.d
mount `cygpath -w ${TMP}` /tmp
mount -c /

if [ ! -e /home/${USER} ] ; then
	ln -s ${HOME} /home/${USER}
fi
