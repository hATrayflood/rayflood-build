#!/bin/sh

if [ "${RF_MSVCVERSION}" ] ; then
	echo "Mozilla build environment: MSVC version ${RF_MSVCVERSION}"
	export PATH=`cygpath -pu "${PATH_MSVC}"`:${PATH}:`cygpath -pu "${PATH_SYS}"`
	export CYGWIN=nontsec
	export RF_TOOLKIT=`cygpath -pu "${RF_TOOLKIT}"`
	if [ -z "${RF_MOZ_TOOLS_SUFFIX}" ] ; then
		export PATH=${RF_TOOLKIT}/extratools/uname:${PATH}
	fi
	alias less='less -r'
fi
