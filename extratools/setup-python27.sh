#!/bin/sh

if [ ! -d "${HOME}/python-2.7" ] ; then
VERSION=2.7.8
cd ${HOME}
if [ `uname` = "Darwin" ] ; then
	curl -O https://www.python.org/ftp/python/${VERSION}/Python-${VERSION}.tgz
	CONFIG="--enable-universalsdk --with-universal-archs=intel"
else
	sudo apt-get -y install libsqlite3-dev
	wget https://www.python.org/ftp/python/${VERSION}/Python-${VERSION}.tgz
fi
tar zxf Python-${VERSION}.tgz
cd Python-${VERSION}
./configure --prefix=${HOME}/python-2.7 ${CONFIG}
make -j4
make install
cd ${HOME}
rm -rf Python-${VERSION}*
fi
