#!/bin/sh

if [ "${RF_MSVCVERSION}" ] ; then
	echo "Mozilla build environment: MSVC version ${RF_MSVCVERSION}"
	export RF_TOOLKIT=`(cd "${RF_TOOLKIT}" && pwd)`
	alias less='less -r'
fi
