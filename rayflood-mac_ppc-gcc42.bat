#!/bin/sh

 CC=gcc-4.2
CXX=g++-4.2
. $(cd $(dirname ${0}) && pwd)/rayflood-mac.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
