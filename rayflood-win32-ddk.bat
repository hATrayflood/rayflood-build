@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_DDKDIR%"=="" (
	echo Microsoft Windows DDK 3790.1830 was not found. Exiting.
	pause
	exit /b 1
)
if "%RF_PSDKDIR%"=="" (
	echo Microsoft Platform SDK for Windows Server 2003 SP1 or R2 was not found. Exiting.
	pause
	exit /b 1
)

echo Setting environment for using Microsoft Windows DDK x86 tools.
PATH=%RF_DDKDIR%\bin\x86;%RF_PSDKDIR%\Bin;%PATH%
set LIB=%RF_PSDKDIR%\Lib;%RF_DDKDIR%\lib\crt\i386;%RF_DDKDIR%\lib\atl\i386;%RF_DDKDIR%\lib\mfc\i386;%~dp0\comsupp_compat\x86;%LIB%
set INCLUDE=%RF_PSDKDIR%\Include;%RF_PSDKDIR%\Include\crt;%RF_DDKDIR%\inc\atl30;%RF_DDKDIR%\inc\mfc42;%INCLUDE%
set CL=-link bufferoverflowu.lib
set LINK=bufferoverflowu.lib

call %~dp0\rayflood-startbash.bat %*

@endlocal
