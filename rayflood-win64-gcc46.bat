@setlocal
@echo off

set RF_MOZ_TOOLS_SUFFIX=-180compat
set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

echo Setting environment for using Mingw GCC46.
rem call %~dp0\extratools\gcc-mingw\tdm64-gcc46\mingw64.bat
call %~dp0\extratools\gcc-mingw\mingw-w64-gcc46\mingw64.bat

call %~dp0\rayflood-startbash.bat %*

@endlocal
