@setlocal
@echo off

set RF_MOZ_TOOLS_SUFFIX=-180compat
set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

echo Setting environment for using CLANG32.
rem call %~dp0\extratools\gcc-mingw\clang32\mingw32.bat
call %~dp0\extratools\gcc-mingw\mingw-w64-clang32\mingw32-dw2.bat

call %~dp0\rayflood-startbash.bat %*

@endlocal
