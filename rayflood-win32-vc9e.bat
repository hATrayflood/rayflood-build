@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_VC9EDIR%"=="" (
	echo Microsoft Visual C++ 2008 Express was not found. Exiting.
	pause
	exit /b 1
)

call "%RF_VC9EDIR%\Bin\vcvars32.bat"
call %~dp0\rayflood-atlmfc-wdk6.bat

ml -? >nul 2>nul
if %ERRORLEVEL% equ 0 (
	goto skipml
)
echo ml was not found. use Microsoft Windows Driver Kits.
if "%RF_WDK6DIR%"=="" (
	echo Microsoft Windows Driver Kits 6.0 6000 or 6.1 6001.18002 was not found. Exiting.
	pause
	exit /b 1
)
set PATH=%PATH%;%RF_WDK6DIR%\bin\x86
:skipml

call %~dp0\rayflood-startbash.bat %*

@endlocal
