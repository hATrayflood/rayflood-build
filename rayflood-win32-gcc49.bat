@setlocal
@echo off

set RF_MOZ_TOOLS_SUFFIX=-180compat
set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

echo Setting environment for using Mingw GCC49.
rem call %~dp0\extratools\gcc-mingw\mingw-gcc49\mingw32.bat

rem call %~dp0\extratools\gcc-mingw\mingw-w64-gcc49-threads-posix\mingw32.bat
rem call %~dp0\extratools\gcc-mingw\mingw-w64-gcc49-threads-posix-dw2\mingw32.bat
call %~dp0\extratools\gcc-mingw\mingw-w64-gcc49-threads-win32\mingw32.bat
rem call %~dp0\extratools\gcc-mingw\mingw-w64-gcc49-threads-win32-dw2\mingw32.bat

call %~dp0\rayflood-startbash.bat %*

@endlocal
