@setlocal
@echo off

set RF_MOZ_TOOLS_SUFFIX=-180compat
set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

echo Setting environment for using Mingw GCC44.
rem call %~dp0\extratools\gcc-mingw\mingw-gcc44\mingw32.bat
call %~dp0\extratools\gcc-mingw\tdm-gcc44\mingw32.bat
rem call %~dp0\extratools\gcc-mingw\tdm-gcc44-dw2\mingw32-dw2.bat

call %~dp0\rayflood-startbash.bat %*

@endlocal
