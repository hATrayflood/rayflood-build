#!/bin/sh

RELEASE=`lsb_release -r | awk '{print $2}'`
CPU_ARCH=`uname -m | sed -e 's/i686/i386/'`

if [ "${RELEASE}" = "10.04" ] ; then
if [ "${RF_UPPER_TOOLCHAIN}" ] ; then
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc49"
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc48"
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc47"
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc46"
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc45"
else
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc44"
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc43"
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc42"
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc41"
TARGETS="${TARGETS} linux_${CPU_ARCH}-llvmgcc42"
fi
elif [ "${RELEASE}" = "12.04" ] ; then
if [ "${RF_UPPER_TOOLCHAIN}" ] ; then
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc49"
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc48"
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc47"
TARGETS="${TARGETS} linux_${CPU_ARCH}-clang36"
TARGETS="${TARGETS} linux_${CPU_ARCH}-clang35"
else
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc46"
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc45"
TARGETS="${TARGETS} linux_${CPU_ARCH}-clang34"
TARGETS="${TARGETS} linux_${CPU_ARCH}-clang33"
TARGETS="${TARGETS} linux_${CPU_ARCH}-clang32"
TARGETS="${TARGETS} linux_${CPU_ARCH}-clang31"
TARGETS="${TARGETS} linux_${CPU_ARCH}-clang30"
fi
elif [ "${RELEASE}" = "14.04" ] ; then
if [ "${RF_UPPER_TOOLCHAIN}" ] ; then
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc49"
else
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc48"
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc47"
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc46"
TARGETS="${TARGETS} linux_${CPU_ARCH}-gcc45"
TARGETS="${TARGETS} linux_${CPU_ARCH}-clang36"
TARGETS="${TARGETS} linux_${CPU_ARCH}-clang35"
TARGETS="${TARGETS} linux_${CPU_ARCH}-clang34"
TARGETS="${TARGETS} linux_${CPU_ARCH}-clang33"
TARGETS="${TARGETS} linux_${CPU_ARCH}-clang32"
fi
fi

export RF_TOOLKIT=$(cd $(dirname ${0}) && pwd)
export RF_ALLOWDIST=1
python /s/rayflood-build/buildshell/parallelbuild.py \
	4 4 \
	/bin/bash \
	/s/rayflood-build/buildshell \
	/s/rayflood-build/buildshell/apps.sh \
	/s/rayflood-build/rayflood \
	${TARGETS}
/usr/lib/indicator-session/gtk-logout-helper --restart &
