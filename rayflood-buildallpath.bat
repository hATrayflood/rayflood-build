set RF_TOOLKIT=%~dp0
set RF_TOOLKIT=%RF_TOOLKIT:~0,-1%

set RF_PA=%PROCESSOR_ARCHITEW6432%
if "%RF_PA%"=="" (
	set RF_PA=%PROCESSOR_ARCHITECTURE%
)
if "%RF_PA%"=="x86" (
	set RF_HKLMSW=HKLM\SOFTWARE
	set RF_CMD=%SystemRoot%\system32\cmd.exe
)
if "%RF_PA%"=="AMD64" (
	set RF_HKLMSW=HKLM\SOFTWARE\Wow6432Node
	set RF_CMD=%SystemRoot%\SysWOW64\cmd.exe
)

set RF_MOZBUILD_DIR=%~d0\mozilla-build
set RF_REGKEY=%RF_HKLMSW%\Cygnus Solutions\Cygwin\mounts v2\/
set RF_REGQRY=reg query "%RF_REGKEY%" /v "native"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_CYGWINDIR=%%B
	)
)

if "%RF_USE_CYGWIN%" == "" (
	set RF_SHELLDIR=%RF_MOZBUILD_DIR%\msys
) else (
	set RF_SHELLDIR=%RF_CYGWINDIR%
)

set RF_REGKEY=HKLM\SOFTWARE\Python\PythonCore\2.5\InstallPath
set RF_REGQRY=reg query "%RF_REGKEY%" /ve
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_PYTHONDIR=%%B
	)
)
set RF_REGKEY=HKLM\SOFTWARE\Python\PythonCore\2.6\InstallPath
set RF_REGQRY=reg query "%RF_REGKEY%" /ve
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_PYTHONDIR=%%B
	)
)
set RF_REGKEY=HKLM\SOFTWARE\Python\PythonCore\2.7\InstallPath
set RF_REGQRY=reg query "%RF_REGKEY%" /ve
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_PYTHONDIR=%%B
	)
)

set RF_REGKEY=HKLM\SOFTWARE\Microsoft\MicrosoftSDK\InstalledSDKs\D2FF9F89-8AA2-4373-8A31-C838BF4DBBE1
set RF_REGQRY=reg query "%RF_REGKEY%" /v "Install Dir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=3* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_PSDKR2DIR=%%B
		set RF_PSDKDIR=%%B
	)
)
set RF_REGKEY=HKLM\SOFTWARE\Microsoft\MicrosoftSDK\InstalledSDKs\8F9E5EF3-A9A5-491B-A889-C58EFFECE8B3
set RF_REGQRY=reg query "%RF_REGKEY%" /v "Install Dir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=3* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_PSDKSP1DIR=%%B
		set RF_PSDKDIR=%%B
	)
)

set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v6.1
set RF_REGQRY=reg query "%RF_REGKEY%" /v "InstallationFolder"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK61DIR=%%k
		set RF_SDK6DIR=%%k
		set RF_VC9DIR=
	)
)
set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v6.0A
set RF_REGQRY=reg query "%RF_REGKEY%" /v "InstallationFolder"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK60ADIR=%%k
		set RF_SDK6DIR=%%k
	)
)
set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v6.0
set RF_REGQRY=reg query "%RF_REGKEY%" /v "InstallationFolder"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK60DIR=%%k
		set RF_SDK6DIR=%%k
	)
)

set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v7.1
set RF_REGQRY=reg query "%RF_REGKEY%" /v "InstallationFolder"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK71DIR=%%k
		set RF_SDK7DIR=%%k
		set RF_VC10DIR=
	)
)
set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v7.0A
set RF_REGQRY=reg query "%RF_REGKEY%" /v "InstallationFolder"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK70ADIR=%%k
		set RF_SDK7DIR=%%k
	)
)
set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v7.0
set RF_REGQRY=reg query "%RF_REGKEY%" /v "InstallationFolder"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK70DIR=%%k
		set RF_SDK7DIR=%%k
		set RF_VC9DIR=
	)
)

set RF_MAPIDIR=%~d0\Office 2010 Developer Resources\Outlook 2010 MAPI Headers
if not exist "%RF_MAPIDIR%" (
	set RF_MAPIDIR=
)

set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Windows Kits\Installed Roots
set RF_REGQRY=reg query "%RF_REGKEY%" /v "KitsRoot"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK80DIR=%%k
	)
)

set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Windows Kits\Installed Roots
set RF_REGQRY=reg query "%RF_REGKEY%" /v "KitsRoot81"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK81DIR=%%k
	)
)
