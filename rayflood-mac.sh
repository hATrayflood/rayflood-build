#!/bin/sh

export RF_TOOLKIT=$(cd $(dirname ${0}) && pwd)
export RF_MSVCVERSION=`echo ${0##*/}`
export RF_MSVCVERSION=`echo ${RF_MSVCVERSION#*-}`
export RF_MSVCVERSION=`echo ${RF_MSVCVERSION%.*}`
export JAVA_HOME=`/usr/libexec/java_home -v 1.6 2>/dev/null`
export AUTOCONF=autoconf213
export PATH=${PATH}:/opt/moztools/bin
export PATH=${PATH}:${RF_TOOLKIT}/extratools/pymake

if ! which ${CC} > /dev/null 2>&1 ; then
if [ "${CC}" = "gcc-4.0" ] ; then
	 CC=gcc-apple-4.0
	CXX=g++-apple-4.0
fi
if [ "${CC}" = "gcc-4.2" ] ; then
	 CC=gcc-apple-4.2
	CXX=g++-apple-4.2
fi

for RF_MP_PATH in /opt/llvm-legacy/bin /opt/llvm-mp/bin ; do
if ${RF_MP_PATH}/${CC} --version > /dev/null 2>&1 ; then
	export PATH=${RF_MP_PATH}:${PATH}
	break
fi
done
fi

CPU_ARCH=${RF_MSVCVERSION#*_}
export CPU_ARCH=${CPU_ARCH%-*}

case ${CC} in
gcc-mp-*|dragonegg-*)
	export  CC="${CC}  `echo ${CPU_ARCH} | sed -e 's/i386/-m32/' | sed -e 's/x86_64/-m64/'`"
	export CXX="${CXX} `echo ${CPU_ARCH} | sed -e 's/i386/-m32/' | sed -e 's/x86_64/-m64/'`"
;;
*)
	export  CC="${CC}  -arch ${CPU_ARCH}"
	export CXX="${CXX} -arch ${CPU_ARCH}"
;;
esac
export  HOST_CC=${CC}
export HOST_CXX=${CXX}

export PS1="${RF_MSVCVERSION}:\W \u\$ "
