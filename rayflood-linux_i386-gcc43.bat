#!/bin/sh

export  CC=gcc-4.3
export CXX=g++-4.3
. $(cd $(dirname ${0}) && pwd)/rayflood-linux.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
