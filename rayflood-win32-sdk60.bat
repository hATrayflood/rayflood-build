@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_SDK60DIR%"=="" (
	echo Microsoft Windows SDK v6.0 was not found. Exiting.
	pause
	exit /b 1
)
if "%RF_PSDKDIR%"=="" (
	echo Microsoft Platform SDK for Windows Server 2003 SP1 or R2 was not found. Exiting.
	pause
	exit /b 1
)

setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
call "%RF_SDK60DIR%\Bin\SetEnv.Cmd" /Release /x86 /vista
set LIB=%LIB%;%RF_PSDKDIR%\Lib
set INCLUDE=%INCLUDE%;%RF_PSDKDIR%\Include
call %~dp0\rayflood-atlmfc-wdk6.bat

call %~dp0\rayflood-startbash.bat %*

@endlocal
