if "%RF_WDK6DIR%"=="" (
	echo Microsoft Windows Driver Kits 6.0 6000 or 6.1 6001.18002 was not found. use Microsoft Platform SDK.
	call %~dp0\rayflood-atlmfc-psdk.bat
	goto exit
)

set LIB=%LIB%;%RF_WDK6DIR%\lib\atl\%RF_WDK_LIB_SUFFIX%;%RF_WDK6DIR%\lib\mfc\%RF_WDK_LIB_SUFFIX%
set INCLUDE=%INCLUDE%;%RF_WDK6DIR%\inc\atl30;%RF_WDK6DIR%\inc\mfc42

:exit
