#!/bin/sh

export RF_TOOLKIT=$(cd $(dirname ${0}) && pwd)
export RF_MSVCVERSION=`echo ${0##*/}`
export RF_MSVCVERSION=`echo ${RF_MSVCVERSION#*-}`
export RF_MSVCVERSION=`echo ${RF_MSVCVERSION%.*}`
JAVA_ARCH=`echo ${RF_MSVCVERSION%-*}`
JAVA_ARCH=`echo ${JAVA_ARCH#*_}`
if [ "${JAVA_ARCH}" = "x86_64" ] ; then
	JAVA_ARCH=amd64
fi
if [ -d "/usr/lib/jvm/java-6-openjdk-${JAVA_ARCH}" ] ; then
	export JAVA_HOME=/usr/lib/jvm/java-6-openjdk-${JAVA_ARCH}
else
	export JAVA_HOME=/usr/lib/jvm/java-6-openjdk
fi
export MOZ_GLX_IGNORE_BLACKLIST=1
YASM=`which yasm-1 2>/dev/null`
if [ "${YASM}" ] ; then
	export YASM
fi
export PATH=${PATH}:${RF_TOOLKIT}/extratools/pymake
