@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_VC10EDIR%"=="" (
	echo Microsoft Visual C++ 2010 Express was not found. Exiting.
	pause
	exit /b 1
)

call "%RF_VC10EDIR%\Bin\vcvars32.bat"
call %~dp0\rayflood-atlmfc-wdk7.bat

call %~dp0\rayflood-startbash.bat %*

@endlocal
