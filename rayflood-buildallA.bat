@echo off
call rayflood-buildall.bat ^
	win64-vc12 win32-vc12 ^
	win64-vc11 win32-vc11 ^
	win64-vc10 win32-vc10 ^
	win64-vc9  win32-vc9  ^
	win64-vc8  win32-vc8
copy /y rayflood-buildstart.bat \
%SystemRoot%\system32\shutdown -r
