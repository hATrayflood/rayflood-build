if "%RF_PSDKDIR%"=="" (
	echo Microsoft Platform SDK for Windows Server 2003 SP1 or R2 was not found. Exiting.
	pause
	exit /b 1
)

set INCLUDE=%INCLUDE%;%RF_PSDKDIR%\include\atl;%RF_PSDKDIR%\include\mfc
