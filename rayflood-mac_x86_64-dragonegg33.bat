#!/bin/sh

 CC=dragonegg-3.3-gcc
CXX=dragonegg-3.3-g++
. $(cd $(dirname ${0}) && pwd)/rayflood-mac.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
