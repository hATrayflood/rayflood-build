@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_VC12DIR%"=="" (
	echo Microsoft Visual Studio 2013 was not found. Exiting.
	pause
	exit /b 1
)

if "%RF_MAPIDIR%"=="" (
	echo Outlook 2010 MAPI Headers was not found. Exiting.
	pause
	exit /b 1
)

rem call "%RF_VC12DIR%\Bin\x86_amd64\vcvarsx86_amd64.bat"
call "%RF_VC12DIR%\Bin\amd64\vcvars64.bat"
set INCLUDE=%RF_MAPIDIR%;%INCLUDE%

call %~dp0\rayflood-startbash.bat %*

@endlocal
