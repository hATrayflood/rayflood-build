#!/bin/sh

 CC=dragonegg-3.2-gcc
CXX=dragonegg-3.2-g++
. $(cd $(dirname ${0}) && pwd)/rayflood-mac.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
