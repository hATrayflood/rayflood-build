#!/bin/sh

export  CC=llvm-gcc
export CXX=llvm-g++
export dragonegg_disable_version_check=1
. $(cd $(dirname ${0}) && pwd)/rayflood-linux.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
