@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_SDK60DIR%"=="" (
	echo Microsoft Windows SDK v6.0 was not found. Exiting.
	pause
	exit /b 1
)
if "%RF_WDK6DIR%"=="" (
	echo Microsoft Windows Driver Kits 6.0 6000 or 6.1 6001.18002 was not found. Exiting.
	pause
	exit /b 1
)
if "%RF_PSDKDIR%"=="" (
	echo Microsoft Platform SDK for Windows Server 2003 SP1 or R2 was not found. Exiting.
	pause
	exit /b 1
)

setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
call "%RF_SDK60DIR%\Bin\SetEnv.Cmd" /Release /x64 /vista
set LIB=%LIB%;%RF_PSDKDIR%\Lib\AMD64
set INCLUDE=%INCLUDE%;%RF_PSDKDIR%\Include
call %~dp0\rayflood-atlmfc-wdk6.bat
set PATH=%PATH%;%RF_WDK6DIR%\bin\x86\amd64

call %~dp0\rayflood-startbash.bat %*

@endlocal
