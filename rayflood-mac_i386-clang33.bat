#!/bin/sh

 CC=clang-mp-3.3
CXX=clang++-mp-3.3
. $(cd $(dirname ${0}) && pwd)/rayflood-mac.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
