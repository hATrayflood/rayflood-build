@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_VC6DIR%"=="" (
	echo Microsoft Visual C++ 6.0 was not found. Exiting.
	pause
	exit /b 1
)

call "%RF_VC6DIR%\Bin\vcvars32.bat"

call %~dp0\rayflood-startbash.bat %*

@endlocal
