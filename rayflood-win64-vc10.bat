@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_VC10DIR%"=="" (
	echo Microsoft Visual Studio 2010 was not found. Exiting.
	pause
	exit /b 1
)

set PATH=%PATH%;%RF_VC10DIR%\Bin\amd64
call "%RF_VC10DIR%\Bin\x86_amd64\vcvarsx86_amd64.bat"

call %~dp0\rayflood-startbash.bat %*

@endlocal
