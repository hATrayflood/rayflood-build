@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_SDK70DIR%"=="" (
	echo Microsoft Windows SDK v7.0 was not found. Exiting.
	pause
	exit /b 1
)

setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
call "%RF_SDK70DIR%\Bin\SetEnv.Cmd" /Release /x64 /win7
call %~dp0\rayflood-atlmfc-wdk7.bat

call %~dp0\rayflood-startbash.bat %*

@endlocal
