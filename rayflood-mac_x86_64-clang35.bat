#!/bin/sh

 CC=clang-mp-3.5
CXX=clang++-mp-3.5
. $(cd $(dirname ${0}) && pwd)/rayflood-mac.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
