@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_VC14EDIR%"=="" (
	echo Microsoft Visual C++ 2015 Express was not found. Exiting.
	pause
	exit /b 1
)

if "%RF_MAPIDIR%"=="" (
	echo Outlook 2010 MAPI Headers was not found. Exiting.
	pause
	exit /b 1
)

call "%RF_VC14EDIR%\bin\x86_amd64\vcvarsx86_amd64.bat"
set INCLUDE=%RF_MAPIDIR%;%INCLUDE%

call %~dp0\rayflood-startbash.bat %*

@endlocal
