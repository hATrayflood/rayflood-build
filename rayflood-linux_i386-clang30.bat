#!/bin/sh

export  CC=clang-3.0
export CXX=clang++-3.0
. $(cd $(dirname ${0}) && pwd)/rayflood-linux.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
