#!/bin/sh

export  CC=clang-3.2
export CXX=clang++-3.2
. $(cd $(dirname ${0}) && pwd)/rayflood-linux.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
