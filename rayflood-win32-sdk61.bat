@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_SDK61DIR%"=="" (
	echo Microsoft Windows SDK v6.1 was not found. Exiting.
	pause
	exit /b 1
)

setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
call "%RF_SDK61DIR%\Bin\SetEnv.Cmd" /Release /x86 /2008
call %~dp0\rayflood-atlmfc-wdk6.bat

call %~dp0\rayflood-startbash.bat %*

@endlocal
