#!/bin/sh

 CC=clang-mp-2.9
CXX=clang++-mp-2.9
. $(cd $(dirname ${0}) && pwd)/rayflood-mac.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
