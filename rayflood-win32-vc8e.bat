@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_VC8EDIR%"=="" (
	echo Microsoft Visual C++ 2005 Express was not found. Exiting.
	pause
	exit /b 1
)
if "%RF_PSDKDIR%"=="" (
	echo Microsoft Platform SDK for Windows Server 2003 SP1 or R2 was not found. Exiting.
	pause
	exit /b 1
)

set PATH=%RF_PSDKDIR%\bin;%PATH%
set LIB=%RF_PSDKDIR%\lib;%LIB%
set INCLUDE=%RF_PSDKDIR%\include;%INCLUDE%
call "%RF_VC8EDIR%\Bin\vcvars32.bat"
call %~dp0\rayflood-atlmfc-psdk.bat

call %~dp0\rayflood-startbash.bat %*

@endlocal
