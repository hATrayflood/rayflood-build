#!/bin/sh

 CC=clang-mp-3.0
CXX=clang++-mp-3.0
. $(cd $(dirname ${0}) && pwd)/rayflood-mac.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
