set RF_TOOLKIT=%~dp0
set RF_TOOLKIT=%RF_TOOLKIT:~0,-1%

for /f "tokens=2,3 delims=-" %%i in ("%RF_BATCHNAME%") do (
	set RF_OS=%%i
	set RF_VC=%%j
)
set RF_MSVCVERSION=%RF_OS%-%RF_VC%
if "%RF_OS%"=="win64" (
	set MOZ_TOOLS=%~dp0\moztools-x64%RF_MOZ_TOOLS_SUFFIX%
	set MIDL_FLAGS=-x64
	set RF_LIB_HKLMSW=HKLM\SOFTWARE
	set RF_WDK_LIB_SUFFIX=amd64
) else (
	set MOZ_TOOLS=%~dp0\moztools%RF_MOZ_TOOLS_SUFFIX%
	set MIDL_FLAGS=-win32 -no_robust
	set RF_LIB_HKLMSW=HKLM\SOFTWARE\Wow6432Node
	set RF_WDK_LIB_SUFFIX=i386
)

set RF_PA=%PROCESSOR_ARCHITEW6432%
if "%RF_PA%"=="" (
	set RF_PA=%PROCESSOR_ARCHITECTURE%
)
if "%RF_PA%"=="x86" (
	set RF_LIB_HKLMSW=HKLM\SOFTWARE
	set RF_HKLMSW=HKLM\SOFTWARE
	set RF_CMD=%SystemRoot%\system32\cmd.exe
)
if "%RF_PA%"=="AMD64" (
	set RF_HKLMSW=HKLM\SOFTWARE\Wow6432Node
	set RF_CMD=%SystemRoot%\SysWOW64\cmd.exe
)

set RF_MOZBUILD_DIR=%~d0\mozilla-build
set RF_REGKEY=%RF_HKLMSW%\Cygnus Solutions\Cygwin\mounts v2\/
set RF_REGQRY=reg query "%RF_REGKEY%" /v "native"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_CYGWINDIR=%%B
	)
)

set RF_7ZIPDIR=%RF_MOZBUILD_DIR%\7zip
set RF_REGKEY=HKLM\SOFTWARE\Wow6432Node\7-Zip
set RF_REGQRY=reg query "%RF_REGKEY%" /v "Path"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_7ZIPDIR=%%B
	)
)
set RF_REGKEY=HKLM\SOFTWARE\7-Zip
set RF_REGQRY=reg query "%RF_REGKEY%" /v "Path"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_7ZIPDIR=%%B
	)
)

if exist "%RF_MOZBUILD_DIR%\python25" (
	set RF_PYTHONDIR=%RF_MOZBUILD_DIR%\python25
)
if exist "%RF_MOZBUILD_DIR%\python" (
	set RF_PYTHONDIR=%RF_MOZBUILD_DIR%\python
)

set RF_NSISDIR=%RF_MOZBUILD_DIR%\nsis-2.22
set RF_REGKEY=%RF_HKLMSW%\NSIS
set RF_REGQRY=reg query "%RF_REGKEY%" /ve
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_NSISDIR=%%B
	)
)

set RF_REGKEY=%RF_LIB_HKLMSW%\JavaSoft\Java Development Kit
set RF_REGQRY=reg query "%RF_REGKEY%" /v "CurrentVersion"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_JDKCURVER=%%B
	)
)
set RF_REGKEY=%RF_REGKEY%\%RF_JDKCURVER%
set RF_REGQRY=reg query "%RF_REGKEY%" /v "JavaHome"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set JAVA_HOME=%%B
	)
)

set RF_REGKEY=%RF_HKLMSW%\Microsoft\VisualStudio\14.0\Setup\VC
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC14DIR=%%B
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\VisualStudio\12.0\Setup\VC
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC12DIR=%%B
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\VisualStudio\11.0\Setup\VC
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC11DIR=%%B
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\VisualStudio\10.0\Setup\VC
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC10DIR=%%B
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\VisualStudio\9.0\Setup\VC
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC9DIR=%%B
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\VisualStudio\8.0\Setup\VC
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC8DIR=%%B
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\VisualStudio\7.1\Setup\VC
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC71DIR=%%B
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\VisualStudio\7.0\Setup\VC
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC7DIR=%%B
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\VisualStudio\6.0\Setup\Microsoft Visual C++
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC6DIR=%%B
	)
)

set RF_REGKEY=%RF_HKLMSW%\Microsoft\VisualStudio\14.0\Setup\wdexpress
set RF_REGQRY=reg query "%RF_REGKEY%" /ve
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set "RF_VC14EDIR=%RF_VC14DIR%"
		set RF_VC14DIR=
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\VCExpress\12.0\Setup\VC
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC12EDIR=%%B
		set RF_VC12DIR=
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\VCExpress\11.0\Setup\VC
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC11EDIR=%%B
		set RF_VC11DIR=
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\VCExpress\10.0\Setup\VC
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC10EDIR=%%B
		set RF_VC10DIR=
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\VCExpress\9.0\Setup\VC
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC9EDIR=%%B
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\VCExpress\8.0\Setup\VC
set RF_REGQRY=reg query "%RF_REGKEY%" /v "ProductDir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=2* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_VC8EDIR=%%B
	)
)

set RF_REGKEY=HKLM\SOFTWARE\Microsoft\MicrosoftSDK\InstalledSDKs\D2FF9F89-8AA2-4373-8A31-C838BF4DBBE1
set RF_REGQRY=reg query "%RF_REGKEY%" /v "Install Dir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=3* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_PSDKR2DIR=%%B
		set RF_PSDKDIR=%%B
	)
)
set RF_REGKEY=HKLM\SOFTWARE\Microsoft\MicrosoftSDK\InstalledSDKs\8F9E5EF3-A9A5-491B-A889-C58EFFECE8B3
set RF_REGQRY=reg query "%RF_REGKEY%" /v "Install Dir"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=3* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_PSDKSP1DIR=%%B
		set RF_PSDKDIR=%%B
	)
)

set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v6.1
set RF_REGQRY=reg query "%RF_REGKEY%" /v "InstallationFolder"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK61DIR=%%k
		set RF_SDK6DIR=%%k
		set "RF_SDK61_VCDIR=%RF_VC9DIR%"
		set RF_VC9DIR=
	)
)
set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v6.0A
set RF_REGQRY=reg query "%RF_REGKEY%" /v "InstallationFolder"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK60ADIR=%%k
		set RF_SDK6DIR=%%k
	)
)
set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v6.0
set RF_REGQRY=reg query "%RF_REGKEY%" /v "InstallationFolder"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK60DIR=%%k
		set RF_SDK6DIR=%%k
		set "RF_SDK60_VCDIR=%%k\VC"
	)
)

set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v7.1
set RF_REGQRY=reg query "%RF_REGKEY%" /v "InstallationFolder"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK71DIR=%%k
		set RF_SDK7DIR=%%k
		set "RF_SDK71_VCDIR=%RF_VC10DIR%"
		set RF_VC10DIR=
	)
)
set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v7.0A
set RF_REGQRY=reg query "%RF_REGKEY%" /v "InstallationFolder"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK70ADIR=%%k
		set RF_SDK7DIR=%%k
	)
)
set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v7.0
set RF_REGQRY=reg query "%RF_REGKEY%" /v "InstallationFolder"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK70DIR=%%k
		set RF_SDK7DIR=%%k
		set "RF_SDK70_VCDIR=%RF_VC9DIR%"
		set RF_VC9DIR=
	)
)

set RF_MAPIDIR=%~d0\Office 2010 Developer Resources\Outlook 2010 MAPI Headers
if not exist "%RF_MAPIDIR%" (
	set RF_MAPIDIR=
)

set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Windows Kits\Installed Roots
set RF_REGQRY=reg query "%RF_REGKEY%" /v "KitsRoot"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK80DIR=%%k
	)
)

set RF_REGKEY=HKLM\SOFTWARE\Microsoft\Windows Kits\Installed Roots
set RF_REGQRY=reg query "%RF_REGKEY%" /v "KitsRoot81"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_SDK81DIR=%%k
	)
)

set RF_REGKEY=HKLM\SOFTWARE\Microsoft\WINDDK\3790.1830
set RF_REGQRY=reg query "%RF_REGKEY%" /v "LFNDirectory"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=3* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_DDKDIR=%%A
	)
)

set RF_REGKEY=%RF_HKLMSW%\Microsoft\.NETFramework
set RF_REGQRY=reg query "%RF_REGKEY%" /v "sdkInstallRootv1.1"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=1,2*" %%i in ('%RF_REGQRY%') do (
		set RF_DOTNETSDKDIR=%%k
	)
)

set RF_REGKEY=%RF_HKLMSW%\Microsoft\KitSetup\configured-kits\{B4285279-1846-49B4-B8FD-B9EAF0FF17DA}\{68656B6B-555E-5459-5E5D-6363635E5F61}
set RF_REGQRY=reg query "%RF_REGKEY%" /v "setup-install-location"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=3* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_WDK71DIR=%%A
		set RF_WDK7DIR=%%A
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\KitSetup\configured-kits\{B4285279-1846-49B4-B8FD-B9EAF0FF17DA}\{676E6B70-5659-5459-5B5F-6063635E5F61}
set RF_REGQRY=reg query "%RF_REGKEY%" /v "setup-install-location"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=3* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_WDK70DIR=%%A
		set RF_WDK7DIR=%%A
	)
)
set RF_REGKEY=%RF_HKLMSW%\Microsoft\KitSetup\configured-kits\{B4285279-1846-49B4-B8FD-B9EAF0FF17DA}\{515A5454-555D-5459-5B5D-616264656660}
set RF_REGQRY=reg query "%RF_REGKEY%" /v "setup-install-location"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=3* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_WDK61DIR=%%A
		set RF_WDK6DIR=%%A
	)
)
set RF_REGKEY=HKLM\SOFTWARE\Microsoft\WINDDK\6000\Setup
set RF_REGQRY=reg query "%RF_REGKEY%" /v "BUILD"
%RF_REGQRY% >nul 2>nul
if %ERRORLEVEL% equ 0 (
	for /f "tokens=3* delims=	 " %%A in ('%RF_REGQRY%') do (
		set RF_WDK60DIR=%%A
		set RF_WDK6DIR=%%A
	)
)

set PATH_SYS=%PATH%
set PATH=
cd /d %SystemRoot%\system32
