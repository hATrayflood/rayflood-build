@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_VC71DIR%"=="" (
	echo Microsoft Visual C++ .NET 2003 was not found. Exiting.
	pause
	exit /b 1
)

call "%RF_VC71DIR%\Bin\vcvars32.bat"

call %~dp0\rayflood-startbash.bat %*

@endlocal
