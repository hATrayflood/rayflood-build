#!/bin/sh

 CC=clang-mp-3.4
CXX=clang++-mp-3.4
. $(cd $(dirname ${0}) && pwd)/rayflood-mac.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
