#!/bin/sh

export  CC=gcc-4.1
export CXX=g++-4.1
. $(cd $(dirname ${0}) && pwd)/rayflood-linux.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
