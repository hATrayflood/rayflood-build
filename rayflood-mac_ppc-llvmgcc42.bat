#!/bin/sh

 CC=llvm-gcc-4.2
CXX=llvm-g++-4.2
. $(cd $(dirname ${0}) && pwd)/rayflood-mac.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
