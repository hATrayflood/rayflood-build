#!/bin/sh

export  CC=llvm-gcc
export CXX=llvm-g++
export LOCAL_INCLUDES=-I/usr/include/i386-linux-gnu
. $(cd $(dirname ${0}) && pwd)/rayflood-linux.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
