#!/bin/sh

export  CC=clang-2.9
export CXX=clang++-2.9
. $(cd $(dirname ${0}) && pwd)/rayflood-linux.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
