@echo off
set HOME=
set RF_ALLOWDIST=1
call rayflood-buildallpath.bat
"%RF_PYTHONDIR%\python" S:\rayflood-build\buildshell\parallelbuild.py ^
	4 4 ^
	"%RF_CMD% /c %RF_SHELLDIR%\bin\bash" ^
	/s/rayflood-build/buildshell ^
	S:\rayflood-build\buildshell\apps.sh ^
	"%~dp0%\rayflood" ^
	%*
