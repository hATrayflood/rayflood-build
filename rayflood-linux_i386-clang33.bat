#!/bin/sh

export  CC=clang-3.3
export CXX=clang++-3.3
. $(cd $(dirname ${0}) && pwd)/rayflood-linux.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
