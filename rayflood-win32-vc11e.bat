@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_VC11EDIR%"=="" (
	echo Microsoft Visual C++ 2012 Express was not found. Exiting.
	pause
	exit /b 1
)

if "%RF_MAPIDIR%"=="" (
	echo Outlook 2010 MAPI Headers was not found. Exiting.
	pause
	exit /b 1
)

call "%RF_VC11EDIR%\Bin\vcvars32.bat"
set INCLUDE=%RF_MAPIDIR%;%INCLUDE%

call %~dp0\rayflood-startbash.bat %*

@endlocal
