@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_SDK71DIR%"=="" (
	echo Microsoft Windows SDK v7.1 was not found. Exiting.
	pause
	exit /b 1
)

setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
call "%RF_SDK71DIR%\Bin\SetEnv.Cmd" /Release /x64 /win7
call %~dp0\rayflood-atlmfc-wdk7.bat
set CL=

call %~dp0\rayflood-startbash.bat %*

@endlocal
