if "%RF_WDK7DIR%"=="" (
	echo Microsoft Windows Driver Kits 7.0 7600.16385.0 or 7.1 7600.16385.1 was not found. use Microsoft Platform SDK.
	call %~dp0\rayflood-atlmfc-psdk.bat
	goto exit
)

set LIB=%LIB%;%RF_WDK7DIR%\lib\ATL\%RF_WDK_LIB_SUFFIX%;%RF_WDK7DIR%\lib\Mfc\%RF_WDK_LIB_SUFFIX%
set INCLUDE=%INCLUDE%;%RF_WDK7DIR%\inc\atl71;%RF_WDK7DIR%\inc\mfc42

:exit
