@setlocal
@echo off

set RF_MOZ_TOOLS_SUFFIX=-180compat
set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

echo Setting environment for using CLANG29.
call %~dp0\extratools\gcc-mingw\clang29\mingw32.bat

call %~dp0\rayflood-startbash.bat %*

@endlocal
