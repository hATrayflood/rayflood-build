@setlocal
@echo off

set RF_MOZ_TOOLS_SUFFIX=-180compat
set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

echo Setting environment for using Mingw GCC47.
rem call %~dp0\extratools\gcc-mingw\mingw-gcc47\mingw32.bat
rem call %~dp0\extratools\gcc-mingw\tdm-gcc47\mingw32.bat
rem call %~dp0\extratools\gcc-mingw\tdm-gcc47-dw2\mingw32-dw2.bat

call %~dp0\extratools\gcc-mingw\mingw-w64-gcc47\mingw32.bat
rem call %~dp0\extratools\gcc-mingw\mingw-w64-gcc47\mingw32-dw2.bat

call %~dp0\rayflood-startbash.bat %*

@endlocal
