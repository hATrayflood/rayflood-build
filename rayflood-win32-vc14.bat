@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_VC14DIR%"=="" (
	echo Microsoft Visual Studio 2015 was not found. Exiting.
	pause
	exit /b 1
)

if "%RF_MAPIDIR%"=="" (
	echo Outlook 2010 MAPI Headers was not found. Exiting.
	pause
	exit /b 1
)

call "%RF_VC14DIR%\bin\vcvars32.bat"
set INCLUDE=%RF_MAPIDIR%;%INCLUDE%

call %~dp0\rayflood-startbash.bat %*

@endlocal
