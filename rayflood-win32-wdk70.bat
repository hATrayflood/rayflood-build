@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_WDK70DIR%"=="" (
	echo Microsoft Windows Driver Kits 7.0 7600.16385.0 was not found. Exiting.
	pause
	exit /b 1
)
if "%RF_PSDKDIR%"=="" (
	echo Microsoft Platform SDK for Windows Server 2003 SP1 or R2 was not found. Exiting.
	pause
	exit /b 1
)

rem call "%RF_WDK70DIR%\bin\setenv.bat" %RF_WDK70DIR% fre x86 WIN7 no_oacr
set PATH=%RF_WDK70DIR%\bin\x86\x86;%RF_WDK70DIR%\bin\x86;%RF_PSDKDIR%\Bin;%PATH%
set LIB=%RF_WDK70DIR%\lib\win7\i386;%RF_WDK70DIR%\lib\Crt\i386;%RF_WDK70DIR%\lib\ATL\i386;%RF_WDK70DIR%\lib\Mfc\i386;%~dp0\comsupp_compat\x86-mt;%LIB%
set INCLUDE=%RF_WDK70DIR%\inc\api;%RF_WDK70DIR%\inc\crt;%RF_WDK70DIR%\inc\api\crt\stl60;%RF_WDK70DIR%\inc\atl71;%RF_WDK70DIR%\inc\mfc42;%RF_PSDKDIR%\Include;%INCLUDE%
set USE_STATIC_LIBS=1

call %~dp0\rayflood-startbash.bat %*

@endlocal
