#!/bin/sh

RELEASE=`xcodebuild -version | grep ^Xcode | awk '{print $2}'`
case ${RELEASE} in
3.*)
	THREADS=2
	CPU_ARCHS="x86_64 i386 powerpc"
	COMPILERS="llvmgcc42 gcc42 gcc40"
;;
4.1*)
	THREADS=4
	CPU_ARCHS="x86_64 i386"
	COMPILERS="clang36 clang35 clang34 clang33 clang32 clang31 clang30 clang29 clang llvmgcc42 gcc42"
;;
4.*)
	THREADS=4
	CPU_ARCHS="x86_64 i386"
	COMPILERS="clang36 clang35 clang34 clang33 clang32 clang31 clang30 clang29 clang llvmgcc42"
;;
5.*)
	THREADS=4
	CPU_ARCHS="x86_64 i386"
	COMPILERS="clang36 clang35 clang34 clang33 clang32 clang31 clang30 clang29 clang"
;;
6.*)
	THREADS=4
	CPU_ARCHS="x86_64 i386"
	COMPILERS="clang36 clang35 clang34 clang33 clang"
;;
esac

OS_MAJOR=`uname -r | awk -F . '{print $1}'`
if [ ${OS_MAJOR} -ge 13 ] ; then
	for LEGACY_CLANG in clang32 clang31 clang30 clang29 ; do
		COMPILERS=`echo ${COMPILERS} | sed -e s/${LEGACY_CLANG}//g`
	done
fi

for CPU_ARCH in ${CPU_ARCHS} ; do
for COMPILER in ${COMPILERS} ; do
	TARGETS="${TARGETS} mac_${CPU_ARCH}-${COMPILER}"
done
done

export RF_TOOLKIT=$(cd $(dirname ${0}) && pwd)
export RF_ALLOWDIST=1
python /s/rayflood-build/buildshell/parallelbuild.py \
	${THREADS} ${THREADS} \
	/bin/bash \
	/s/rayflood-build/buildshell \
	/s/rayflood-build/buildshell/apps.sh \
	/s/rayflood-build/rayflood \
	${TARGETS}
