#!/bin/sh

 CC=clang-mp-3.2
CXX=clang++-mp-3.2
. $(cd $(dirname ${0}) && pwd)/rayflood-mac.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
