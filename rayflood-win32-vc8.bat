@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_VC8DIR%"=="" (
	echo Microsoft Visual Studio 2005 was not found. Exiting.
	pause
	exit /b 1
)

call "%RF_VC8DIR%\Bin\vcvars32.bat"

call %~dp0\rayflood-startbash.bat %*

@endlocal
