#!/bin/sh

 CC=gcc-mp-4.7
CXX=g++-mp-4.7
. $(cd $(dirname ${0}) && pwd)/rayflood-mac.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
