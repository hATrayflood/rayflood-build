#!/bin/bash

. ${RF_MOZCONFIG}/lightning-sunbird.sh
. ${RF_MOZCONFIG}/gecko-2.0.sh

RF_SRCDIR="comm-2.0"
RF_L10N="ja"
RF_SRC_SUBDIR=mozilla

ac_add_options --enable-calendar
RF_LIGHTNING_TARGET=tb+sm
