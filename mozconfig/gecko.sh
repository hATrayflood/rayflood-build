#!/bin/bash

RF_FTPDIR=${RF_PROJECT}
case ${RF_PROJECT} in
lightning-sunbird|sunbird)
	RF_FTPDIR=calendar/sunbird
;;
lightning)
	RF_FTPDIR=calendar/lightning
;;
fennec)
	RF_FTPDIR=mobile
;;
esac

if [ "${RF_PROJECT}" = "mozilla" ] ; then
	if [ "${RF_CANDIDATE}" ] ; then
		RF_SRC_URL=http://archive.mozilla.org/pub/${RF_FTPDIR}/candidates/${RF_VERSION}-candidates/${RF_CANDIDATE}/${RF_APP_VER}-source.tar.bz2
	else
		RF_SRC_URL=http://archive.mozilla.org/pub/${RF_FTPDIR}/releases/${RF_PROJECT}${RF_VERSION}/source/${RF_APP_VER}-source.tar.bz2
	fi
elif [ "${RF_PROJECT}" = "seamonkey" ] && [ "${RF_L10NREP}" = "-" ] ; then
	if [ "${RF_CANDIDATE}" ] ; then
		RF_SRC_URL=http://archive.mozilla.org/pub/${RF_FTPDIR}/nightly/candidates-${RF_VERSION}/${RF_APP_VER}.source.tar.bz2
	else
		RF_SRC_URL=http://archive.mozilla.org/pub/${RF_FTPDIR}/releases/${RF_VERSION}/${RF_APP_VER}.source.tar.bz2
	fi
elif [ "${RF_L10NREP}" = "-" ] ; then
	if [ "${RF_CANDIDATE}" ] ; then
		RF_SRC_URL=http://archive.mozilla.org/pub/${RF_FTPDIR}/nightly/${RF_VERSION}-candidates/${RF_CANDIDATE}/${RF_APP_VER}-source.tar.bz2
	else
		RF_SRC_URL=http://archive.mozilla.org/pub/${RF_FTPDIR}/releases/${RF_VERSION}/source/${RF_APP_VER}-source.tar.bz2
	fi
else
	if [ "${RF_CANDIDATE}" ] ; then
		RF_SRC_URL=http://archive.mozilla.org/pub/${RF_FTPDIR}/nightly/${RF_VERSION}-candidates/${RF_CANDIDATE}/source/${RF_APP_VER}.source.tar.bz2
	else
		RF_SRC_URL=http://archive.mozilla.org/pub/${RF_FTPDIR}/releases/${RF_VERSION}/source/${RF_APP_VER}.source.tar.bz2
	fi
fi

if [ -z "${RF_CANDIDATE}" ] ; then
if [ "${RF_APP_VER}" = "firefox-3.0.19" ] ; then
	RF_SRC_URL=http://archive.mozilla.org/pub/${RF_FTPDIR}/releases/${RF_VERSION}-real-real/source/${RF_APP_VER}-source.tar.bz2
fi
fi

ac_add_options --disable-debug
ac_add_options --disable-tests

case ${RF_MSVCVERSION} in
linux_*)
	ac_add_options --enable-strip
;;
#mac_*-gcc*)
#	ac_add_options --enable-strip
#;;
esac
