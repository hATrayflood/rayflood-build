#!/bin/bash

RF_GECKO_BRANCH=192
RF_GECKO_WINSDK_VER=7
RF_GECKO_MIN_WINVER=500
RF_L10NREP="l10n-mozilla-1.9.2"

. ${RF_MOZCONFIG}/gecko.sh

ac_add_options --enable-libxul

case ${RF_MSVCVERSION} in
win32-*)
	ac_add_options --enable-ipc
	ac_add_options --enable-tree-freetype
	#ac_add_options --enable-ddraw-surface
	#. ${RF_MOZCONFIG}/setdxsdkdir.sh
	case ${RF_MSVCVERSION} in
	*-vc9)
		ac_add_options --enable-jemalloc
	;;
	*-vc8)
		ac_add_options --enable-jemalloc
	;;
	esac
;;
linux_*)
	ac_add_options --enable-ipc
;;
esac
