#!/bin/bash

. ${RF_MOZCONFIG}/xulrunner.sh
. ${RF_MOZCONFIG}/gecko-1.9.0.sh

RF_L10N="ja zh-TW"

ac_add_options --enable-extensions=default,python

case ${RF_MSVCVERSION} in
win32-*)
	case ${RF_MSVCVERSION} in
	*-llvmgcc42)
		ac_add_options --disable-javaxpcom
	;;
	*-gcc4)
		ac_add_options --disable-javaxpcom
	;;
	esac
;;
esac
