#!/bin/bash

ac_add_options --enable-application=mail
. ${RF_MOZCONFIG}/thunderbird.sh
. ${RF_MOZCONFIG}/gecko-10.0.sh

RF_SRCDIR="comm-esr10"
RF_L10N="ja zh-TW"
RF_SRC_SUBDIR=mozilla

case ${RF_MSVCVERSION} in
mac_*)
	case ${RF_MSVCVERSION} in
	*-clang29)
		RF_DONTBUILD=1
	;;
	esac
;;
esac
