#!/bin/bash

. ${RF_MOZCONFIG}/firefox.sh
. ${RF_MOZCONFIG}/gecko-2.0.sh

RF_SRCDIR="mozilla-2.0"
RF_L10N="ja zh-TW"

case ${RF_MSVCVERSION} in
win32-*)
	case ${RF_MSVCVERSION} in
	*-vc10)
		RF_PGO=1
	;;
	*-vc9)
		RF_PGO=1
	;;
	esac
;;
win64-*)
	case ${RF_MSVCVERSION} in
	*-vc10)
		RF_PGO=1
	;;
	esac
;;
linux_*)
	case ${RF_MSVCVERSION} in
	*-gcc45)
		RF_PGO=1
	;;
	*-gcc46)
		RF_PGO=1
	;;
	esac
;;
#mac_*)
#	RF_PGO=1
#;;
esac
