#!/bin/bash

. ${RF_MOZCONFIG}/thunderbird.sh
. ${RF_MOZCONFIG}/gecko-1.8.1.sh

RF_L10N="ja zh-TW"

ac_add_options --enable-extensions=default,inspector
