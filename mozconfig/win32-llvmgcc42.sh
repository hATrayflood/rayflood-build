#!/bin/bash

RF_OPT="-O1"
RF_OPT_CPU="-march=core2"
RF_BUILD_MAX_GECKO=190

ac_add_options --enable-strip
ac_add_options --disable-accessibility
ac_add_options --disable-oji
ac_add_options --disable-installer
