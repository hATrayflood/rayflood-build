#!/bin/bash

. ${RF_MOZCONFIG}/lightning-sunbird.sh
. ${RF_MOZCONFIG}/gecko-1.9.1.sh

RF_SRCDIR="comm-1.9.1"
RF_L10N="ja"
RF_SRC_SUBDIR=mozilla

ac_add_options --enable-calendar
RF_LIGHTNING_TARGET=tb+sm

if [ -e "../extra-chrome/proxiesfix-seamonkey" ] ; then
	rm -rf ../extra-chrome/proxiesfix-seamonkey
fi
