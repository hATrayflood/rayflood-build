#!/bin/bash

if [ -f ${topsrcdir}/b2g/config/mozconfig ] ; then
	. ${topsrcdir}/b2g/config/mozconfig
elif [ -d ${topsrcdir}/b2g/xul ] ; then
	ac_add_options --enable-application=b2g/xul
else
	ac_add_options --enable-application=b2g
fi
