#!/bin/bash

RF_OPT="-O2"
RF_OPT_CPU="-G7 -arch:SSE2"
RF_BUILD_MAX_GECKO=191

if [ $RF_GECKO_BRANCH -ge 190 ] ; then
	RF_OPT_CPUOFF=1
fi
