#!/bin/bash

. ${RF_MOZCONFIG}/seamonkey.sh
. ${RF_MOZCONFIG}/gecko-17.0.sh

RF_SRCDIR="comm-esr17"
RF_L10N="ja"
RF_SRC_SUBDIR=mozilla
PROFILE_GEN_SCRIPT="python ${RF_OBJDIR}/${RF_SRC_SUBDIR}/_profile/pgo/profileserver.py"
ac_add_options --enable-extensions=default,-venkman,-inspector,-irc

case ${RF_MSVCVERSION} in
win32-*)
	case ${RF_MSVCVERSION} in
	*-vc11)
		RF_PGO=1
	;;
	*-vc10)
		RF_PGO=1
	;;
	*-vc9)
		RF_PGO=1
	;;
	esac
;;
win64-*)
	case ${RF_MSVCVERSION} in
	*-vc11)
		RF_PGO=1
	;;
	*-vc10)
		RF_PGO=1
	;;
	esac
;;
linux_*)
	case ${RF_MSVCVERSION} in
	*-gcc48)
		RF_PGO=1
	;;
	*-gcc47)
		RF_PGO=1
	;;
	*-gcc46)
		RF_PGO=1
	;;
	*-gcc45)
		RF_PGO=1
	;;
	esac
;;
mac_*)
	case ${RF_MSVCVERSION} in
	*-clang29)
		RF_DONTBUILD=1
	;;
	*-clang31)
		RF_DONTBUILD=1
	;;
	esac
;;
esac
