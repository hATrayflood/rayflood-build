#!/bin/bash

. ${RF_MOZCONFIG}/firefox.sh
. ${RF_MOZCONFIG}/gecko-1.9.2.sh

RF_SRCDIR="mozilla-1.9.2"
RF_L10N="ja zh-TW"
PROFILE_GEN_SCRIPT="python ${RF_OBJDIR}/${RF_SRC_SUBDIR}/_profile/pgo/profileserver.py"

case ${RF_MSVCVERSION} in
win32-*)
	case ${RF_MSVCVERSION} in
	*-vc10)
		RF_PGO=1
	;;
	*-vc9)
		RF_PGO=1
	;;
	esac
;;
#linux_*|mac_*)
#	RF_PGO=1
#;;
esac
