#!/bin/bash

unixpath(){
	which cygpath > /dev/null 2>&1
	if [ $? -eq 0 ] ; then
		cygpath -a -u "${1}"
	else
		echo "/`echo "${1}" | tr \\\\ / | tr -d :`"
	fi
}

if [ -z "${RF_INPROGRESS}" ] ; then
	if [ "${RF_SDK6DIR}" ] ; then
		case ${RF_MSVCVERSION} in
		win64-*)
			RF_BIN_SUFFIX=\\x64
			RF_LIB_SUFFIX=\\x64
		;;
		esac
		export PATH="`unixpath \"${RF_SDK6DIR}\\bin${RF_BIN_SUFFIX}\"`:${PATH}"
		export LIB="${RF_SDK6DIR}Lib${RF_LIB_SUFFIX};${LIB}"
		export INCLUDE="${RF_SDK6DIR}Include;${INCLUDE}"
		export RF_MSVC_WINSDK_VER_REAL=6
		export WINDOWSSDKDIR="${RF_SDK6DIR}"
		RF_BIN_SUFFIX=
		RF_LIB_SUFFIX=
	else
		echo "RF_SDK6DIR is missing."
		echo "Please install Microsoft Windows SDK 6.0 or 6.1 ."
		RF_DONTBUILD=1
	fi
fi
