#!/bin/bash

. ${RF_MOZCONFIG}/thunderbird.sh
. ${RF_MOZCONFIG}/gecko-1.8.0.sh

RF_SRC_URL="${RF_SRC_URL} http://rayflood.org/mozilla/thunderbird-1.5-InfoPlist.tar.bz2"

RF_L10N="ja"

ac_add_options --enable-extensions=default,inspector
