#!/bin/bash

. ${RF_MOZCONFIG}/mozilla.sh
. ${RF_MOZCONFIG}/gecko-1.7.sh

RF_SRC_URL="${RF_SRC_URL} http://rayflood.org/mozilla/mozilla-1.7-ja-JP.tar.bz2"
RF_SRC_URL="${RF_SRC_URL} http://rayflood.org/mozilla/mozilla-1.7-zh-TW.tar.bz2"

ac_add_options --enable-crypto
