#!/bin/bash

if [ -z "${RF_INPROGRESS}" ] ; then
	if [ "${DXSDK_DIR}" ] ; then
		case ${RF_MSVCVERSION} in
		win64-*)
			RF_LIB_SUFFIX=\\x64
		;;
		*)
			RF_LIB_SUFFIX=\\x86
		;;
		esac
		export LIB="${LIB};${DXSDK_DIR}Lib${RF_LIB_SUFFIX}"
		export INCLUDE="${INCLUDE};${DXSDK_DIR}Include"
		RF_LIB_SUFFIX=
	else
		echo "DXSDK_DIR is missing."
		echo "Please install Microsoft DirectX SDK (February 2010) ."
		RF_DONTBUILD=1
	fi
fi
