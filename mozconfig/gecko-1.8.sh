#!/bin/bash

. ${RF_MOZCONFIG}/gecko.sh

RF_SRC_URL="${RF_SRC_URL} http://rayflood.org/mozilla/gecko-1.8-inspector-ja.tar.bz2"

#ac_add_options --disable-shared
#ac_add_options --enable-static

ac_add_options --enable-svg
ac_add_options --enable-canvas

case ${RF_MSVCVERSION} in
linux_*)
	ac_add_options --enable-system-cairo
;;
esac
