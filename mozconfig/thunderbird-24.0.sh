#!/bin/bash

ac_add_options --enable-application=mail
. ${RF_MOZCONFIG}/thunderbird.sh
. ${RF_MOZCONFIG}/gecko-24.0.sh

RF_SRCDIR="comm-esr24"
RF_L10N="ja zh-TW"
RF_SRC_SUBDIR=mozilla
ac_add_options --enable-calendar
RF_LIGHTNING_TARGET=tb+sm
RF_LIGHTNING_VERSION=`cat ${BUILDROOT}/${RF_APP_VER}/${RF_SRCDIR}/calendar/sunbird/config/version.txt 2>/dev/null`

case ${RF_MSVCVERSION} in
mac_*)
	case ${RF_MSVCVERSION} in
	*-clang29)
		RF_DONTBUILD=1
	;;
	*-clang31)
		RF_DONTBUILD=1
	;;
	esac
;;
esac
