#!/bin/bash

. ${RF_MOZCONFIG}/firefox.sh
. ${RF_MOZCONFIG}/gecko-10.0.sh

RF_SRCDIR="mozilla-esr10"
RF_L10N="ja zh-TW"
PROFILE_GEN_SCRIPT="python ${RF_OBJDIR}/${RF_SRC_SUBDIR}/_profile/pgo/profileserver.py"

case ${RF_MSVCVERSION} in
win32-*)
	case ${RF_MSVCVERSION} in
	*-vc11)
		RF_PGO=1
	;;
	*-vc10)
		RF_PGO=1
	;;
	*-vc9)
		RF_PGO=1
	;;
	esac
;;
win64-*)
	case ${RF_MSVCVERSION} in
	*-vc11)
		RF_PGO=1
	;;
	*-vc10)
		RF_PGO=1
	;;
	esac
;;
linux_*)
	case ${RF_MSVCVERSION} in
	*-gcc48)
		RF_PGO=1
	;;
	*-gcc47)
		RF_PGO=1
	;;
	*-gcc46)
		RF_PGO=1
	;;
	*-gcc45)
		RF_PGO=1
	;;
	esac
;;
#mac_*)
#	RF_PGO=1
#;;
esac
