#!/bin/bash

ac_add_options --enable-application=mail
. ${RF_MOZCONFIG}/thunderbird.sh
. ${RF_MOZCONFIG}/gecko-1.9.1.sh

RF_SRCDIR="comm-1.9.1"
RF_L10N="ja zh-TW"
RF_SRC_SUBDIR=mozilla
