#!/bin/bash

. ${RF_MOZCONFIG}/xulrunner.sh
. ${RF_MOZCONFIG}/gecko-1.9.1.sh

RF_SRCDIR="mozilla-1.9.1"
RF_L10N="ja zh-TW"

ac_add_options --enable-extensions=default,python
