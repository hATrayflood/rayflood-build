#!/bin/bash

RF_INSTALLER=browser/installer

. ${RF_MOZCONFIG}/firefox.sh
. ${RF_MOZCONFIG}/gecko-1.7.sh

RF_SRC_URL="${RF_SRC_URL} http://rayflood.org/mozilla/firefox-1.0-ja-JP.tar.bz2"
RF_SRC_URL="${RF_SRC_URL} http://rayflood.org/mozilla/firefox-1.0-ja-JPM.tar.bz2"
RF_SRC_URL="${RF_SRC_URL} http://rayflood.org/mozilla/firefox-1.0-zh-TW.tar.bz2"
