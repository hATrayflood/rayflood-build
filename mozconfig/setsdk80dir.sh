#!/bin/bash

unixpath(){
	which cygpath > /dev/null 2>&1
	if [ $? -eq 0 ] ; then
		cygpath -a -u "${1}"
	else
		echo "/`echo "${1}" | tr \\\\ / | tr -d :`"
	fi
}

if [ -z "${RF_INPROGRESS}" ] ; then
	if [ "${RF_SDK80DIR}" ] && [ "${RF_MAPIDIR}" ] ; then
		case ${RF_MSVCVERSION} in
		win64-*)
			RF_BIN_SUFFIX=\\x64
			RF_LIB_SUFFIX=\\x64
		;;
		win32-*)
			RF_BIN_SUFFIX=\\x86
			RF_LIB_SUFFIX=\\x86
		;;
		esac
		export PATH="`unixpath \"${RF_SDK80DIR}bin${RF_BIN_SUFFIX}\"`:${PATH}"
		export LIB="${RF_SDK80DIR}Lib\win8\um${RF_LIB_SUFFIX};${LIB}"
		export INCLUDE="${RF_MAPIDIR};${RF_SDK80DIR}Include\shared;${RF_SDK80DIR}Include\um;${RF_SDK80DIR}Include\winrt;${INCLUDE}"
		export RF_MSVC_WINSDK_VER_REAL=80
		export WINDOWSSDKDIR="${RF_SDK80DIR}"
		RF_BIN_SUFFIX=
		RF_LIB_SUFFIX=
	elif [ -z "${RF_SDK80DIR}" ] ; then
		echo "RF_SDK80DIR is missing."
		echo "Please install Microsoft Windows SDK 8.0 ."
		RF_DONTBUILD=1
	elif [ -z "${RF_MAPIDIR}" ] ; then
		echo "RF_MAPIDIR is missing."
		echo "Please install Outlook 2010 MAPI Headers ."
		RF_DONTBUILD=1
	fi
fi
