#!/bin/bash

. ${RF_MOZCONFIG}/fennec.sh
. ${RF_MOZCONFIG}/gecko-2.0.sh

RF_SRCDIR="mozilla-2.1"
RF_L10N="ja zh-TW"

ac_add_options --enable-chrome-format=omni

case ${RF_MSVCVERSION} in
win32-*)
	case ${RF_MSVCVERSION} in
	*-vc10)
		#RF_PGO=1
		true
	;;
	*-vc9)
		#RF_PGO=1
		true
	;;
	esac
;;
win64-*)
	case ${RF_MSVCVERSION} in
	*-vc10)
		#RF_PGO=1
		true
	;;
	esac
;;
linux_*)
	case ${RF_MSVCVERSION} in
	*-gcc45)
		#RF_PGO=1
		true
	;;
	*-gcc46)
		#RF_PGO=1
		true
	;;
	esac
;;
#mac_*)
#	RF_PGO=1
#;;
esac
