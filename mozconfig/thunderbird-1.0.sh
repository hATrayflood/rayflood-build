#!/bin/bash

RF_INSTALLER=mail/installer

. ${RF_MOZCONFIG}/thunderbird.sh
. ${RF_MOZCONFIG}/gecko-1.7.sh

RF_SRC_URL="${RF_SRC_URL} http://rayflood.org/mozilla/thunderbird-1.0-ja-JP.tar.bz2"
RF_SRC_URL="${RF_SRC_URL} http://rayflood.org/mozilla/thunderbird-1.0-ja-JPM.tar.bz2"

ac_add_options --enable-extensions=default,inspector
