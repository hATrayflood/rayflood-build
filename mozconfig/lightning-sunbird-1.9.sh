#!/bin/bash

. ${RF_MOZCONFIG}/lightning-sunbird.sh
. ${RF_MOZCONFIG}/gecko-17.0.sh

RF_SRCDIR="comm-esr17"
RF_L10N="ja zh-TW"
RF_SRC_SUBDIR=mozilla

ac_add_options --enable-calendar
RF_LIGHTNING_TARGET=tb+sm
