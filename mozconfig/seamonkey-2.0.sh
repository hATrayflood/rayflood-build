#!/bin/bash

. ${RF_MOZCONFIG}/seamonkey.sh
. ${RF_MOZCONFIG}/gecko-1.9.1.sh

RF_SRCDIR="comm-1.9.1"
RF_L10N="ja"
RF_SRC_SUBDIR=mozilla
PROFILE_GEN_SCRIPT="python ${RF_OBJDIR}/${RF_SRC_SUBDIR}/_profile/pgo/profileserver.py"
ac_add_options --enable-extensions=default,-venkman,-inspector,-irc

case ${RF_MSVCVERSION} in
win32-*)
	RF_SRC_URL="${RF_SRC_URL} http://rayflood.org/mozilla/proxiesfix-seamonkey.tar.bz2"
	case ${RF_MSVCVERSION} in
	*-vc10)
		RF_PGO=1
	;;
	*-vc9)
		RF_PGO=1
	;;
	esac
;;
#linux_*|mac_*)
#	RF_PGO=1
#;;
esac
