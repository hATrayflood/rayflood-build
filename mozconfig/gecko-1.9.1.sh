#!/bin/bash

RF_GECKO_BRANCH=191
RF_GECKO_WINSDK_VER=6
RF_GECKO_MIN_WINVER=500
RF_L10NREP="l10n-mozilla-1.9.1"

. ${RF_MOZCONFIG}/gecko.sh

ac_add_options --enable-libxul

case ${RF_MSVCVERSION} in
win32-*)
	case ${RF_MSVCVERSION} in
	*-vc9)
		ac_add_options --enable-jemalloc
	;;
	*-vc8)
		ac_add_options --enable-jemalloc
	;;
	*-vc71)
		ac_add_options --disable-vista-sdk-requirements
		. ${RF_MOZCONFIG}/setpsdkdir.sh
	;;
	*-vc71e)
		ac_add_options --disable-vista-sdk-requirements
	;;
	*-ddk)
		ac_add_options --disable-vista-sdk-requirements
		ac_add_options --disable-crashreporter
	;;
	esac
;;
esac
