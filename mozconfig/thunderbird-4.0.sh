#!/bin/bash

ac_add_options --enable-application=mail
. ${RF_MOZCONFIG}/thunderbird.sh
. ${RF_MOZCONFIG}/gecko-2.0.sh

RF_SRCDIR="comm-2.0"
RF_L10N="ja"
RF_SRC_SUBDIR=mozilla
