#!/bin/bash

unixpath(){
	which cygpath > /dev/null 2>&1
	if [ $? -eq 0 ] ; then
		cygpath -a -u "${1}"
	else
		echo "/`echo "${1}" | tr \\\\ / | tr -d :`"
	fi
}

if [ -z "${RF_INPROGRESS}" ] ; then
	if [ "${RF_PSDKDIR}" ] ; then
		case ${RF_MSVCVERSION} in
		win64-*)
			RF_LIB_SUFFIX=\\AMD64
		;;
		esac
		export PATH="${PATH}:`unixpath \"${RF_PSDKDIR}\\bin\"`"
		export LIB="${RF_PSDKDIR}Lib${RF_LIB_SUFFIX};${LIB}"
		export INCLUDE="${RF_PSDKDIR}Include;${INCLUDE}"
		RF_LIB_SUFFIX=
	else
		echo "RF_PSDKDIR is missing."
		echo "Please install Microsoft Platform SDK for Windows Server 2003 SP1 or R2 ."
		RF_DONTBUILD=1
	fi
fi
