#!/bin/bash

. ${RF_MOZCONFIG}/b2g.sh
. ${RF_MOZCONFIG}/gecko-17.0.sh

RF_SRCDIR="mozilla-esr17"

ac_add_options --enable-chrome-format=omni

case ${RF_MSVCVERSION} in
win32-*)
	case ${RF_MSVCVERSION} in
	*-vc11)
		#RF_PGO=1
		true
	;;
	*-vc10)
		#RF_PGO=1
		true
	;;
	*-vc9)
		#RF_PGO=1
		true
	;;
	esac
;;
win64-*)
	case ${RF_MSVCVERSION} in
	*-vc11)
		#RF_PGO=1
		true
	;;
	*-vc10)
		#RF_PGO=1
		true
	;;
	esac
;;
linux_*)
	case ${RF_MSVCVERSION} in
	*-gcc48)
		#RF_PGO=1
		true
	;;
	*-gcc47)
		#RF_PGO=1
		true
	;;
	*-gcc46)
		#RF_PGO=1
		true
	;;
	*-gcc45)
		#RF_PGO=1
		true
	;;
	esac
;;
#mac_*)
#	RF_PGO=1
#;;
esac
