#!/bin/bash

if [ -f ${topsrcdir}/mobile/config/mozconfig ] ; then
	. ${topsrcdir}/mobile/config/mozconfig
elif [ -d ${topsrcdir}/mobile/xul ] ; then
	ac_add_options --enable-application=mobile/xul
else
	ac_add_options --enable-application=mobile
fi
