#!/bin/bash

if [ "${RF_OBJDIR}" ] ; then
	if [ -d "${RF_OBJDIR}" ] ; then
		mk_add_options MOZ_OBJDIR=$(cd ${RF_OBJDIR} && pwd)
	else
		mk_add_options MOZ_OBJDIR=${RF_OBJDIR}
	fi
fi

RF_PROJECT=${RF_APP_VER%-*}
RF_VERSION=${RF_APP_VER##*-}

RF_SRCDIR="mozilla"
RF_SRC_SUBDIR=.
RF_L10NREP="-"
RF_GECKO_WINSDK_VER=5
RF_GECKO_MIN_WINVER=400
RF_BUILD_MIN_GECKO=170
RF_BUILD_MAX_GECKO=2400

RF_VERSION_MAJOR=`echo ${RF_VERSION} | awk -F . '{print $1}'`
RF_VERSION_MINOR=`echo ${RF_VERSION} | awk -F . '{print $2}' | sed -e s/esr$//`
if [ ${RF_VERSION_MAJOR} -ge 24 ] ; then
RF_VERSION_MINOR=0
fi
. ${RF_MOZCONFIG}/${RF_PROJECT}-${RF_VERSION_MAJOR}.${RF_VERSION_MINOR}.sh
RF_CONFERR=$?
if [ ${RF_CONFERR} -ne 0 ] ; then
	exit ${RF_CONFERR}
fi
. ${RF_MOZCONFIG}/${RF_MSVCVERSION}.sh
RF_CONFERR=$?
if [ ${RF_CONFERR} -ne 0 ] ; then
	exit ${RF_CONFERR}
fi

if [ ${RF_GECKO_BRANCH} -lt ${RF_BUILD_MIN_GECKO} ] || [ ${RF_GECKO_BRANCH} -gt ${RF_BUILD_MAX_GECKO} ] ; then
	RF_DONTBUILD=1
fi

# under python-2.7.3
if [ -z "${RF_INPROGRESS}" ] ; then
if [ ${RF_GECKO_BRANCH} -ge 2400 ] ; then
if ! which python2.7 > /dev/null ; then
	export PATH=${HOME}/python-2.7/bin:${PATH}
elif [ `python2.7 -c "import sys; print sys.hexversion"` -lt 34014192 ] ; then
	export PATH=${HOME}/python-2.7/bin:${PATH}
fi
fi
fi

if [ "${RF_THREADS}" != 1 ] || [ "${RF_PYMAKE}" ] ; then
	mk_add_options MOZ_MAKE_FLAGS=-j${RF_THREADS}
fi

if [ "${RF_PYMAKE}" ] ; then
	RF_MAKE_CMD="pymake"
else
	RF_MAKE_CMD="make"
fi
if [ "${RF_PGO}" ] && [ ! "${RF_DONT_PGO}" ] && [ ! "${RF_OPT_OFF}" ] ; then
	RF_BUILD_CMD="${RF_MAKE_CMD} -f client.mk profiledbuild"
	mk_add_options PROFILE_GEN_SCRIPT="${PROFILE_GEN_SCRIPT}"
else
	RF_BUILD_CMD="${RF_MAKE_CMD} -f client.mk build"
fi

if [ "${RF_L10N}" ] ; then
	case ${RF_MSVCVERSION} in
	mac_*)
		RF_L10N=`echo ${RF_L10N} | sed -e 's/ja/ja-JP-mac ja/g'`
	;;
	*)
		RF_L10N=`echo ${RF_L10N} | sed -e 's/ja/ja ja-JP-mac/g'`
	;;
	esac
	ac_add_options --enable-ui-locale=`echo ${RF_L10N} | awk '{print $1}'`
	ac_add_options --with-l10n-base=../l10n
fi

if [ "${RF_OPT_OFF}" ] ; then
	ac_add_options --disable-optimize
elif [ "${RF_OPT_DEFAULT}" ] ; then
	ac_add_options --enable-optimize
else
if [ "${RF_OPT_CPUOFF}" ] ; then
if [ "${RF_OPT_CPUOFF_FLAG}" ] ; then
	ac_add_options --enable-optimize="${RF_OPT} ${RF_OPT_CPUOFF_FLAG}"
else
	ac_add_options --enable-optimize="${RF_OPT}"
fi
else
	ac_add_options --enable-optimize="${RF_OPT} ${RF_OPT_CPU}"
fi
fi

if [ "${RF_MSVC_MIN_WINVER}" ] && [ ${RF_GECKO_MIN_WINVER} -lt ${RF_MSVC_MIN_WINVER} ] ; then
	ac_add_options --with-windows-version=${RF_MSVC_MIN_WINVER}
fi
if [ "${RF_MSVC_WINSDK_VER_REAL}" ] ; then
	RF_MSVC_WINSDK_VER=${RF_MSVC_WINSDK_VER_REAL}
fi
if [ "${RF_MSVC_WINSDK_VER}" ] && [ ${RF_GECKO_WINSDK_VER} -gt ${RF_MSVC_WINSDK_VER} ] ; then
	. ${RF_MOZCONFIG}/setsdk${RF_GECKO_WINSDK_VER}dir.sh
fi

case ${RF_MSVCVERSION} in
win*-clang*)
	export C_INCLUDE_PATH="${INCLUDE};${C_INCLUDE_PATH}"
	export CPLUS_INCLUDE_PATH="${INCLUDE};${CPLUS_INCLUDE_PATH}"
	export LIBRARY_PATH="${LIB};${LIBRARY_PATH}"
;;
mac_*)
	RF_XCODE_VERSION=`xcodebuild -version | grep ^Xcode | awk '{print $2}'`
	case ${RF_XCODE_VERSION} in
	3.*)
		case ${RF_MSVCVERSION} in
		*-gcc33)
			RF_MACSDK_VER=3.9
		;;
		*-gcc40)
			RF_MACSDK_VER=4
		;;
		*)
			RF_MACSDK_VER=5
		;;
		esac
	;;
	4.0*|4.1*|4.2*|4.3*)
		RF_MACSDK_VER=6
	;;
	4.4*|4.5*|4.6*)
		RF_MACSDK_VER=7
	;;
	5.*)
		RF_MACSDK_VER=8
	;;
	6.0*|6.1*|6.2*)
		RF_MACSDK_VER=9
	;;
	6.*)
		RF_MACSDK_VER=10
	;;
	esac
	ac_add_options --target=`echo ${CPU_ARCH} | sed -e 's/ppc/powerpc/'`-apple-darwin`echo ${RF_MACSDK_VER}+4.0 | bc`.0
	ac_add_options --with-macos-sdk=`xcodebuild -version -sdk macosx10.${RF_MACSDK_VER} | grep ^Path: | awk '{print $2}'`
;;
esac

if [ -z "${RF_INPROGRESS}" ] ; then
echo
echo RF_THREADS=${RF_THREADS}
echo RF_DONTDIST=${RF_DONTDIST}
echo RF_DONT_PGO=${RF_DONT_PGO}
echo RF_OPT_OFF=${RF_OPT_OFF}
echo RF_OPT_CPUOFF=${RF_OPT_CPUOFF}
echo RF_OPT_DEFAULT=${RF_OPT_DEFAULT}
fi
