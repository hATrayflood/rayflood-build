#!/bin/bash

case ${RF_VERSION} in
1.0b1*)
	. ${RF_MOZCONFIG}/lightning-sunbird-1.0b1.sh
;;
1.0b2*)
	. ${RF_MOZCONFIG}/lightning-sunbird-1.0b2.sh
;;
1.0b4*)
	. ${RF_MOZCONFIG}/lightning-sunbird-1.0b4.sh
;;
esac
