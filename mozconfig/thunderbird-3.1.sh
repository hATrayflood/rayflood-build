#!/bin/bash

ac_add_options --enable-application=mail
. ${RF_MOZCONFIG}/thunderbird.sh
. ${RF_MOZCONFIG}/gecko-1.9.2.sh

RF_SRCDIR="comm-1.9.2"
RF_L10N="ja zh-TW"
RF_SRC_SUBDIR=mozilla

RF_PATCHES_EXTRA="${RF_PATCHES_EXTRA} calendar-1.9.2-l10n-ja.patch calendar-1.9.2-l10n-ja-JP-mac.patch calendar-1.9.2-l10n-zh-TW.patch"
