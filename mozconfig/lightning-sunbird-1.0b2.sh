#!/bin/bash

. ${RF_MOZCONFIG}/lightning-sunbird.sh
. ${RF_MOZCONFIG}/gecko-1.9.2.sh

RF_SRCDIR="comm-1.9.2"
RF_L10N="ja zh-TW"
RF_SRC_SUBDIR=mozilla

ac_add_options --enable-calendar
RF_LIGHTNING_TARGET=tb
