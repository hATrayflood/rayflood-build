#!/bin/bash

. ${RF_MOZCONFIG}/lightning-sunbird.sh
. ${RF_MOZCONFIG}/gecko-10.0.sh

RF_SRCDIR="comm-esr10"
RF_L10N="ja zh-TW"
RF_SRC_SUBDIR=mozilla

ac_add_options --enable-calendar
RF_LIGHTNING_TARGET=tb+sm
