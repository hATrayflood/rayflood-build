#!/bin/bash

RF_OPT="-O1"
RF_OPT_CPU="-march=prescott"
RF_BUILD_MAX_GECKO=190

ac_add_options --enable-strip
ac_add_options --disable-accessibility
