#!/bin/bash

RF_GECKO_BRANCH=190
RF_GECKO_WINSDK_VER=6
RF_GECKO_MIN_WINVER=500

. ${RF_MOZCONFIG}/gecko.sh

ac_add_options --disable-mochitest
ac_add_options --enable-libxul
#ac_add_options --disable-libxul

case ${RF_MSVCVERSION} in
win32-*)
	case ${RF_MSVCVERSION} in
	*-vc9)
		ac_add_options --enable-jemalloc
	;;
	*-vc8)
		ac_add_options --enable-jemalloc
	;;
	*-vc71)
		ac_add_options --disable-vista-sdk-requirements
		. ${RF_MOZCONFIG}/setpsdkdir.sh
	;;
	*-vc71e)
		ac_add_options --disable-vista-sdk-requirements
	;;
	*-ddk)
		ac_add_options --disable-vista-sdk-requirements
		ac_add_options --disable-crashreporter
	;;
	*-clang32)
		ac_add_options --disable-vista-sdk-requirements
	;;
	*-clang31)
		ac_add_options --disable-vista-sdk-requirements
	;;
	*-clang29)
		ac_add_options --disable-vista-sdk-requirements
	;;
	*-llvmgcc42)
		ac_add_options --disable-vista-sdk-requirements
	;;
	*-gcc4*)
		ac_add_options --disable-vista-sdk-requirements
	;;
	esac
;;
win64-*)
	case ${RF_MSVCVERSION} in
	*-gcc4*)
		ac_add_options --disable-vista-sdk-requirements
	;;
	esac
;;
esac
