#!/bin/bash

RF_OPT="-O3"

RELEASE=`xcodebuild -version | grep ^Xcode | awk '{print $2}'`
case ${RELEASE} in
4.1*|4.2*)
	RF_OPT_CPU="-march=prescott"
	RF_BUILD_MIN_GECKO=1000
	RF_BUILD_MAX_GECKO=1700
;;
4.*)
	RF_OPT_CPU="-march=core2"
	RF_BUILD_MIN_GECKO=1000
;;
5.*)
	RF_OPT_CPU="-march=core2"
	RF_BUILD_MIN_GECKO=1000
;;
6.*)
	RF_OPT_CPU="-march=core2"
	RF_BUILD_MIN_GECKO=1000
;;
esac
