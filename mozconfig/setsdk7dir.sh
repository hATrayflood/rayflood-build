#!/bin/bash

unixpath(){
	which cygpath > /dev/null 2>&1
	if [ $? -eq 0 ] ; then
		cygpath -a -u "${1}"
	else
		echo "/`echo "${1}" | tr \\\\ / | tr -d :`"
	fi
}

if [ -z "${RF_INPROGRESS}" ] ; then
	if [ "${RF_SDK7DIR}" ] ; then
		case ${RF_MSVCVERSION} in
		win64-*)
			RF_BIN_SUFFIX=\\x64
			RF_LIB_SUFFIX=\\x64
		;;
		esac
		export PATH="`unixpath \"${RF_SDK7DIR}\\bin${RF_BIN_SUFFIX}\"`:${PATH}"
		export LIB="${RF_SDK7DIR}Lib${RF_LIB_SUFFIX};${LIB}"
		export INCLUDE="${RF_SDK7DIR}Include;${INCLUDE}"
		export RF_MSVC_WINSDK_VER_REAL=7
		export WINDOWSSDKDIR="${RF_SDK7DIR}"
		RF_BIN_SUFFIX=
		RF_LIB_SUFFIX=
	else
		echo "RF_SDK7DIR is missing."
		echo "Please install Microsoft Windows SDK 7.0 or 7.1 ."
		RF_DONTBUILD=1
	fi
fi
