#!/bin/bash

unixpath(){
	which cygpath > /dev/null 2>&1
	if [ $? -eq 0 ] ; then
		cygpath -a -u "${1}"
	else
		echo "/`echo "${1}" | tr \\\\ / | tr -d :`"
	fi
}

if [ -z "${RF_INPROGRESS}" ] ; then
	if [ "${RF_SDK81DIR}" ] && [ "${RF_MAPIDIR}" ] ; then
		case ${RF_MSVCVERSION} in
		win64-*)
			RF_BIN_SUFFIX=\\x64
			RF_LIB_SUFFIX=\\x64
		;;
		win32-*)
			RF_BIN_SUFFIX=\\x86
			RF_LIB_SUFFIX=\\x86
		;;
		esac
		export PATH="`unixpath \"${RF_SDK81DIR}bin${RF_BIN_SUFFIX}\"`:${PATH}"
		export LIB="${RF_SDK81DIR}Lib\winv6.3\um${RF_LIB_SUFFIX};${LIB}"
		export INCLUDE="${RF_MAPIDIR};${RF_SDK81DIR}Include\shared;${RF_SDK81DIR}Include\um;${RF_SDK81DIR}Include\winrt;${INCLUDE}"
		export RF_MSVC_WINSDK_VER_REAL=81
		export WINDOWSSDKDIR="${RF_SDK81DIR}"
		RF_BIN_SUFFIX=
		RF_LIB_SUFFIX=
	elif [ -z "${RF_SDK81DIR}" ] ; then
		echo "RF_SDK81DIR is missing."
		echo "Please install Microsoft Windows SDK 8.1 ."
		RF_DONTBUILD=1
	elif [ -z "${RF_MAPIDIR}" ] ; then
		echo "RF_MAPIDIR is missing."
		echo "Please install Outlook 2010 MAPI Headers ."
		RF_DONTBUILD=1
	fi
fi
