#!/bin/bash

. ${RF_MOZCONFIG}/xulrunner.sh
. ${RF_MOZCONFIG}/gecko-2.0.sh

RF_SRCDIR="mozilla-2.0"
RF_L10N="ja zh-TW"

ac_add_options --enable-javaxpcom
