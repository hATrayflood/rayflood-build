#!/bin/bash

RF_GECKO_BRANCH=170

. ${RF_MOZCONFIG}/gecko.sh

#ac_add_options --disable-shared
#ac_add_options --enable-static

RF_SRC_URL="${RF_SRC_URL} http://rayflood.org/mozilla/gecko-libart_lgpl.tar.bz2"

case ${RF_MSVCVERSION} in
win32-*)
	case ${RF_MSVCVERSION} in
	*-vc6|*-gcc*|*-llvmgcc42)
		ac_add_options --enable-svg
		ac_add_options --enable-svg-renderer-libart
		export MOZ_INTERNAL_LIBART_LGPL=1
	;;
	*)
		ac_add_options --enable-svg
		ac_add_options --enable-svg-renderer-gdiplus
	;;
	esac
;;
linux_*)
	ac_add_options --enable-default-toolkit=gtk2
	ac_add_options --enable-xft
	ac_add_options --disable-freetype2
	ac_add_options --enable-svg
	ac_add_options --enable-svg-renderer-libart
	export MOZ_INTERNAL_LIBART_LGPL=1
;;
esac
