#!/bin/bash

RF_GECKO_BRANCH=200
RF_GECKO_WINSDK_VER=7
RF_GECKO_MIN_WINVER=500
RF_L10NREP="l10n-mozilla-2.0"
#RF_PYMAKE=1

. ${RF_MOZCONFIG}/gecko.sh

ac_add_options --enable-libxul
ac_add_options --enable-ipc
ac_add_options --enable-jemalloc

case ${RF_MSVCVERSION} in
win32-*)
	export WIN32_CRT_SRC_DIR=FORCE_NEW_JEMALLOC
	ac_add_options --enable-tree-freetype
;;
win64-*)
	export WIN32_CRT_SRC_DIR=FORCE_NEW_JEMALLOC
	ac_add_options --enable-tree-freetype
	ac_add_options  --build=x86_64-pc-mingw32
	ac_add_options   --host=x86_64-pc-mingw32
	ac_add_options --target=x86_64-pc-mingw32
	case ${RF_MSVCVERSION} in
	*-psdk)
		ac_add_options --disable-methodjit
		ac_add_options --disable-tracejit
		ac_add_options --disable-ctypes
	;;
	*-gcc4)
		ac_add_options --disable-jemalloc
		ac_add_options --disable-ctypes
		ac_add_options --disable-angle
		ac_add_options --with-windows-version=502
	;;
	esac
;;
esac
