@setlocal
@echo off

set RF_BATCHNAME=%~n0
call %~dp0\rayflood-basepath.bat

if "%RF_PSDKDIR%"=="" (
	echo Microsoft Platform SDK for Windows Server 2003 SP1 or R2 was not found. Exiting.
	pause
	exit /b 1
)
if "%RF_VC8EDIR%"=="" (
	echo Microsoft Visual C++ 2005 Express was not found. Exiting.
	pause
	rem exit /b 1
)

call "%RF_PSDKDIR%\SetEnv.Cmd" /X64 /RETAIL
set INCLUDE=%INCLUDE%;%RF_VC8EDIR%\include;%RF_VC8EDIR%\include\sys
set CL=-link bufferoverflowu.lib
set LINK=bufferoverflowu.lib

call %~dp0\rayflood-startbash.bat %*

@endlocal
