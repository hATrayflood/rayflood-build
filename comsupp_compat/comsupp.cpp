/*
	Replacement for the 32 bit comsupp.lib missing from the Microsoft SDK.

	I have no idea what the original issue_error calls did but silently
	ignoring them didn't seem like a good idea.  Feel free to choose your
	own exception or take them out altogether.

	No mystery with ConvertBSTRToString.  Plain and simple.
	
	Both RaiseException and WideCharToMultibyte are system calls.  
	Don't attempt to put any C runtime calls here. IE Profile migration
	will fail if you do.
	
	To use, build the library with the included Makefile and copy it to 
	a directory in your LIB path before the SDK/Lib/IA64 directory.
	No special header files or code modifications are needed.
	
*/

#include <windows.h>
#include <Unknwn.h>

void   __stdcall _com_issue_error(HRESULT hr)
{
	RaiseException(EXCEPTION_NONCONTINUABLE_EXCEPTION, EXCEPTION_NONCONTINUABLE, 
		1, (ULONG_PTR *)&hr);
}

void __stdcall _com_issue_errorex(HRESULT hr, IUnknown* pu, REFIID ref)
{
	RaiseException(EXCEPTION_NONCONTINUABLE_EXCEPTION, EXCEPTION_NONCONTINUABLE, 
		1, (ULONG_PTR *)&hr);
}

namespace _com_util 
{
	char * __stdcall ConvertBSTRToString(BSTR bzIn)
	{
		if(bzIn == NULL) return (NULL);
		//--	Get the length needed for the output string.
		//--	Bail if 0 (it should be at least 1 for the NULL)
		int mblen = WideCharToMultiByte(CP_ACP, 0, bzIn, -1, NULL, 0, NULL, NULL);
		if (mblen == 0) return(NULL);
		//--	Allocate the output string, bail if NULL.
		char * szOut = new char[mblen];
		if (szOut == NULL) return(NULL);
		//--	Do the conversion and compare the number of characters converted
		//--	with the number returned from the first call.
		//--	Delete the string if they're not equal.
		if (WideCharToMultiByte(CP_ACP, 0, bzIn, mblen, szOut, mblen, NULL, NULL) != mblen)
		{
			delete []szOut;
			szOut = NULL;
		}
		return(szOut);
	}
}

