#!/bin/sh

 CC=gcc-4.0
CXX=g++-4.0
. $(cd $(dirname ${0}) && pwd)/rayflood-mac.sh

if [ "$*" ] ; then
	nice -20 $*
	exit $?
else
	bash -i
fi
